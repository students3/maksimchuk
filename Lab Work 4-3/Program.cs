﻿using System;

namespace Lab_Work_4_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Animal[] animalArray = {
                new Animal { Genus = "Monkey", Weight = 10 },
                new Animal { Genus = "Tigger", Weight = 70 },
                new Animal { Genus = "Rabbit", Weight = 3 },
                new Animal { Genus = "Parrot", Weight = 1 },
                new Animal { Genus = "Donkey", Weight = 15 },
                new Animal { Genus = "Jackal", Weight = 22 }
                };

            Animals animalsInstance = new Animals(animalArray);
            Console.WriteLine("Unsorted animals\n");

            foreach (Animal item in animalsInstance)
            {
                Console.WriteLine($"Genius : {item.Genus}, Weight : {item.Weight}");
            }

            Console.WriteLine("\nSorted animals by Weight (Ascending)\n");
            Array.Sort(animalArray, Animal.SortWeightAscending());

            foreach (var item in animalArray)
            {
                Console.WriteLine($"Genius : {item.Genus}, Weight : {item.Weight}");
            }

            Console.WriteLine("\nSorted animals by Genus (Descending)\n");
            Array.Sort(animalArray, Animal.SortGenusDescending());

            foreach (var item in animalArray)
            {
                Console.WriteLine($"Genius : {item.Genus}, Weight : {item.Weight}");
            }
            Console.ReadLine();
        }
    }
}
