﻿using System;
using System.Collections.Generic;

namespace Lab_Work_4_3
{
    public class Animal : IComparable<Animal>
    {
        public Animal()
        {

        }
        public Animal(string genius, int weight)
        {
            this.Genus = genius;
            this.Weight = weight;
        }
        public string Genus { get; set; }
        public int Weight { get; set; }

        public int CompareTo(Animal obj)
        {
            int compareResult = 0;
            Animal compareAnimal = obj;
            compareResult = string.Compare(this.Genus, compareAnimal.Genus);
            return compareResult;
        }
        public static IComparer<Animal> SortWeightAscending()
        {
            return new SortWeightAscendingHelper();
        }
        public static IComparer<Animal> SortGenusDescending()
        {
            return new SortGenusDescendingHelper();
        }
        private class SortWeightAscendingHelper : IComparer<Animal>
        {
            public int Compare(Animal x, Animal y)
            {
                int compareResult = 0;
                Animal animal1 = x;
                Animal animal2 = y;
                compareResult = animal1.Weight - animal2.Weight;
                return compareResult;
            }
        }
        private class SortGenusDescendingHelper : IComparer<Animal>
        {
            public int Compare(Animal x, Animal y)
            {
                int compareResult = 0;
                if (x is Animal && y is Animal)
                {
                    Animal animal1 = x as Animal;
                    Animal animal2 = y as Animal;
                    compareResult = (-1) * animal1.CompareTo(animal2);
                }
                return compareResult;
            }
        }
    }
}

