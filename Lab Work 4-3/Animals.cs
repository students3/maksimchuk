﻿using System.Collections;

namespace Lab_Work_4_3
{
    public class Animals : IEnumerable
    {
        private readonly Animal[] _animals;

        public Animals()
        {
        }

        public Animals(Animal[] animals)
        {
            _animals = animals;
        }

        public IEnumerator GetEnumerator()
        {
            return _animals.GetEnumerator();
        }
    }
}
