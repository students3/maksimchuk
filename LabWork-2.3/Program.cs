﻿using System;

namespace LabWork_2._3
{
    class Program
    {
        static void Main(string[] args)
        {
            var divider = new string('-', 50);
            // 10) declare 2 objects
            Money moneyUAH100 = new Money(100, CurrencyTypes.UAH);
            Money moneyUAH200 = new Money(200, CurrencyTypes.UAH);
            Console.WriteLine($"moneyUAH100 : Amount = {moneyUAH100.Amount} ; Currency = {moneyUAH100.CurrencyTypeEnum}");
            Console.WriteLine($"moneyUAH200 : Amount = {moneyUAH200.Amount} ; Currency = {moneyUAH200.CurrencyTypeEnum}");
            Console.WriteLine(divider);
            // 11) do operations
            // add 2 objects of Money
            Money moneyUAH300 = moneyUAH100 + moneyUAH200;
            Console.WriteLine($"moneyUAH300 = moneyUAH100 + moneyUAH200 : \nmoneyUAH300 : Amount = {moneyUAH300.Amount} ; Currency = {moneyUAH300.CurrencyTypeEnum}");
            Console.WriteLine(divider);
            // add 1st object of Money and double
            Money money15050 = moneyUAH100 + 50.50;
            Console.WriteLine($"moneyUAH15050 = moneyUAH150 + 50.50 : \nmoneyUAH15050 : Amount = {money15050.Amount} ; Currency = {money15050.CurrencyTypeEnum}");
            // decrease 2nd object of Money by 1
            Console.WriteLine(divider);
            moneyUAH200--;
            Console.WriteLine($"moneyUAH200-- : \n moneyUAH199 : Amount = {moneyUAH200.Amount} ; Currency = {moneyUAH200.CurrencyTypeEnum}");
            // increase 1st object of Money
            Console.WriteLine(divider);
            moneyUAH100++;
            Console.WriteLine($"moneyUAH100++ : \n moneyUAH101 : Amount = {moneyUAH100.Amount} ; Currency = {moneyUAH100.CurrencyTypeEnum}");
            Console.WriteLine(divider);
            // compare 2 objects of Money
            Console.WriteLine($"moneyUAH100 <  moneyUAH200 : {moneyUAH100 < moneyUAH200}");
            Console.WriteLine(divider);
            // compare 2nd object of Money and string
            // ???
            // check CurrencyType of every object
            if (moneyUAH100 & moneyUAH200)
            {
                Console.WriteLine("CurrencyType is UAH");
            } else
            {
                Console.WriteLine("CurrencyType isn't UAH");
            }
            // convert 1st object of Money to string
            Console.WriteLine(divider);
            Console.WriteLine("(string)moneyUAH300 : ");
            string moneyString = (string)moneyUAH300;
            Console.WriteLine(moneyString);
            Console.ReadLine();
        }
    }
}
