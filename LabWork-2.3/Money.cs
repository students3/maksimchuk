﻿
namespace LabWork_2._3
{
    // 1) declare enumeration CurrencyTypes, values UAH, USD, EU


    class Money
    {
        // 2) declare 2 properties Amount, CurrencyType
        public double Amount { get; set; }
        public CurrencyTypes CurrencyTypeEnum { get; set; }

        private static string msg = "ERROR: Use of this operator for money of different type is forbidden";

        // 3) declare parameter constructor for properties initialization
        public Money(double amount, CurrencyTypes ctypes)
        {
            this.Amount = amount;
            this.CurrencyTypeEnum = ctypes;
        }

        // 4) declare overloading of operator + to add 2 objects of Money
        public static Money operator +(Money m1, Money m2)
        {
            if (m1 != null && m2 != null && m1.CurrencyTypeEnum == m2.CurrencyTypeEnum)
            {
                return new Money(m1.Amount + m2.Amount, m1.CurrencyTypeEnum);
            }
            else
            {
                System.Console.WriteLine(msg);
                return null;
            }
        }

        public static Money operator +(Money money, double dAmount)
        {
            if (money != null)
            {
                return new Money(money.Amount + dAmount, money.CurrencyTypeEnum);
            }
            else
            {
                return null;
            }
        }

        // 5) declare overloading of operator -- to decrease object of Money by 1
        public static Money operator --(Money money)
        {
            if (money != null)
            {
                return new Money(--money.Amount, money.CurrencyTypeEnum);
            }
            else
            {
                return null;
            }
        }

        public static Money operator ++(Money money)
        {
            if (money != null)
            {
                return new Money(++money.Amount, money.CurrencyTypeEnum);
            } else
            {
                return null;
            }
        }

        // 6) declare overloading of operator * to increase object of Money 3 times
        public static Money operator *(Money m1, Money m2)
        {
            if (m1 != null && m2 != null && m1.CurrencyTypeEnum == m2.CurrencyTypeEnum)
            {
                return new Money(m1.Amount * m2.Amount, m1.CurrencyTypeEnum);
            }
            else
            {
                System.Console.WriteLine(msg);
                return null;
            }
        }

        // 7) declare overloading of operator > and < to compare 2 objects of Money
        public static bool operator >(Money m1, Money m2)
        {
            bool result = false;
            if (m1 != null && m2 != null && m1.CurrencyTypeEnum == m2.CurrencyTypeEnum)
            {
                if (m1.Amount > m2.Amount)
                    result = true;
            }
            else
            {
                System.Console.WriteLine(msg);
            }
            return result;
        }
        public static bool operator <(Money m1, Money m2)
        {
            bool result = false;
            if (m1 != null && m2 != null && m1.CurrencyTypeEnum == m2.CurrencyTypeEnum)
            {
                if (m1.Amount < m2.Amount) result = true;
            }
            else
            {
                System.Console.WriteLine(msg);
            }
            return result;
        }

        // 8) declare overloading of operator true and false to check CurrencyType of object
        public static bool operator true(Money money)
        {
            return true;
        }

        public static bool operator false(Money money)
        {
            return false;
        }

        public static bool operator &(Money money1, Money money2)
        {
            bool result = false;
            if (money1 != null && money2 !=null)
            {
                result = money1.CurrencyTypeEnum.Equals(money2.CurrencyTypeEnum);
            }
            return result;
        }

        // 9) declare overloading of implicit/ explicit conversion  to convert Money to double, string and vice versa
        public static implicit operator string(Money money)
        {
            string impResult = "Amount = " + money.Amount.ToString("N2") + "; Currency = " + money.CurrencyTypeEnum.ToString();
            return impResult;
        }
    }
}
