﻿using System;

namespace Lab_Work_3_2
{
    class Bird
    {
        public int[] FlySpeed = { 5, 15, 25, 35 };
        public int NormalSpeed { get; set; }
        public string Nick { get; set; }
        private bool BirdFlewAway;
        public Bird() { }
        public Bird (string name, int speed)
        {
            this.Nick = name;
            this.NormalSpeed = speed;
        }
        public void FlyAway(int incrmnt)
        {
            if (BirdFlewAway == false)
            {
                NormalSpeed += incrmnt;
                if (NormalSpeed >= FlySpeed[3]) {
                    BirdFlewAwayException e = new BirdFlewAwayException(string.Format("{0} flew with incredible speed!", Nick),
                        "Oh! Startle.", DateTime.Now);
                    e.HelpLink = "http://en.wikipedia.org/wiki/Tufted_titmouse";
                    throw e;

                }
            } else
            {
                Console.WriteLine("BirdFlewAway");
                Console.WriteLine("Bird speed = "+ NormalSpeed);
            }
        }
    }
}
