﻿using System;

namespace Lab_Work_3_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Bird someBird = new Bird("woody woodpecker", 20);
            int increment = 45;
            char rdk;
            do
            {
                try
                {
                    someBird.FlyAway(increment);
                }
                catch (BirdFlewAwayException e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine(e.When);
                    Console.WriteLine(e.Why);
                }
                catch (System.Exception e)
                {
                    Console.WriteLine("CLS exception : Message - " + e.Message + " Source - " + e.Source);
                }
                finally
                {
                    Console.WriteLine("For the next step...");
                }
                Console.WriteLine("Monitoring in Try block ");
                Console.WriteLine("Please, type the number\n1. array overflow\n2. throw exception\n3. user exception");
                uint i = uint.Parse(Console.ReadLine());
                try
                {
                    switch (i)
                    {
                        case 1:
                            someBird.FlySpeed[10] = 10;
                            break;
                        case 2:
                            throw (new System.Exception("Oh! My system exception..."));
                        case 3:
                            throw new StackOverflowException();
                            break;
                        default:
                            break;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                rdk = Console.ReadKey().KeyChar;
            } while (rdk != ' ');
        }
    }
}

