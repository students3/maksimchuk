﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabWork_5._4_Entities
{
    class Program
    {
        static void Main(string[] args)
        {
            DB_work wrk = new DB_work();
            Dictionary<string, string> dict = new Dictionary<string, string>();
            if (wrk.Lecturer_check())
            {
                Console.WriteLine("Ok");
                wrk.Lecturer_find("L_1");
                wrk.Lecturer_phoneUpd("L_1", "123-456-78");
                wrk.Lecturer_entRdr("city", "Hartford", dict);
                Console.WriteLine("Lecturers from Hartford");
                foreach (var item in dict)
                {
                    Console.WriteLine("Lecturer: {0} {1}", item.Key, item.Value);
                }
            }
            else
            { Console.WriteLine("Failure"); }
            Console.ReadKey();
        }
    }
}
