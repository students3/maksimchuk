﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.EntityClient;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace LabWork_5._4_Entities
{
    public class DB_work
    {
        // implement bool lecturer_entRdr(string clmn, string clmn_value, Dictionary<string, string> dict) method to read the  lecturers information

        // implement try-catch block with using block var con = new EntityConnection(@"name = tect_entities_dbEntities")

        // open connection
        // create EntityCommand by CreateCommand() method

        // define CommandText for the select query with where statement clmn column = clmn_value


        // implement using block for EntityDataReader rdr = cmd.ExecuteReader(CommandBehavior.SequentialAccess | CommandBehavior.CloseConnection)

        // create while (rdr.Read())  loop

        // to read
        // var a = rdr.GetString(0);
        // var b = rdr.GetString(1);
        // dict.Add(a, b);

        public bool Lecturer_entRdr(string clmn, string clmn_value, Dictionary<string, string> dict)
        {
            try
            {
                using (var efConnection = new EntityConnection(@"name = tect_entities_dbEntities"))
                {
                    efConnection.Open();
                    EntityCommand cmd = efConnection.CreateCommand();
                    cmd.CommandText = $"SELECT A.lc_fname, A.lc_lname FROM tect_entities_dbEntities.lecturers as A where A.{clmn}"
                                    + $"='{clmn_value}'";
                    using (EntityDataReader reader = cmd.ExecuteReader(CommandBehavior.SequentialAccess | CommandBehavior.CloseConnection))
                    {
                        while (reader.Read())
                        {
                            var a = reader.GetString(0);
                            var b = reader.GetString(1);
                            dict.Add(a, b);
                        }
                        return true;
                    }
                }
            } catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        // implement bool lecturer_check() method to write lecturers
        // implement try-catch block with using block for  var ctx = new tect_entities_dbEntities()
        // to write lecturers in foreach loop
        //Console.WriteLine("Last lecturer: lc_id {0}, name {1} {2}", cur_lecturer.lc_id, cur_lecturer.lc_fname, cur_lecturer.lc_lname);
        public bool Lecturer_check()
        {
            bool checkresult = false;
            try
            {
                var ctx = new tect_datasets_dbEntities();
                foreach (var item in ctx.lecturers)
                {
                    var lecturerInfo = $"Lecturer ID : {item.lc_id} ".PadRight(5) +
                                        $"Lecturer name : {item.lc_fname} {item.lc_lname}";
                    Console.WriteLine(lecturerInfo);
                }
                checkresult = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
            return checkresult;
        }



        // implement bool lecturer_find(string key) method to find the  lecturer by primary key

        // implement try-catch block with using block var ctx = new tect_entities_dbEntities()

        //use ctx.lecturers.Find(key) method call

        public bool Lecturer_find(string key)
        {
            bool checkresult = false;
            try
            {
                var ctx = new tect_datasets_dbEntities();
                var ltr = ctx.lecturers.Find(key);
                if (ltr != null)
                {
                    Console.WriteLine($"Find lecturer: lc_id {ltr.lc_id}, name {ltr.lc_fname} {ltr.lc_lname}");
                    checkresult = true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
            return checkresult;
        }


        // implement bool lecturer_phoneUpd(string key, string phone) method to update the  lecturer phone number
        public bool Lecturer_phoneUpd(string key, string phone)
        {
            bool updateResult = false;
            try
            {
                // implement try-catch block with using block var ctx = new tect_entities_dbEntities()
                var ctx = new tect_datasets_dbEntities();
                //use ctx.lecturers.Find(key) method call
                var ltr = ctx.lecturers.Find(key);
                //assign parameter value to phone property
                if (ltr != null)
                {
                    Console.WriteLine($"Find lecturer: lc_id {ltr.lc_id}, name {ltr.lc_fname} {ltr.lc_lname}");
                    ltr.phone = phone;
                    ctx.SaveChanges();
                    updateResult = true;
                }
                //ctx.SaveChanges();
                //rslt = true;
                }
            catch (Exception e)
            {
                return false;
            }
            return updateResult;
        }

    }
}
