//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LabWork_5._4_Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class rating
    {
        public string course_id { get; set; }
        public Nullable<decimal> bonus { get; set; }
        public Nullable<decimal> rate { get; set; }
    }
}
