﻿using System;

namespace LabWork_2._2_factorial_
{
    class Program
    {
        static void Main(string[] args)
        {
            //Define parameters to calculate the factorial of
            //Call fact() method to calculate
            Console.WriteLine("Fact(5) = " + fact(5));
            Console.ReadLine();
        }

        //Create fact() method  with parameter to calculate factorial
        //Use ternary operator
        static int fact(int f)
        {
            // TODO: use ternary operator !!! (Done)
            return f == 0 ? 1 : f * (fact(f - 1));
        }
    }
}
