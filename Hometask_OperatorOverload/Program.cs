﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hometask_OperatorOverload.Models;

namespace Hometask_OperatorOverload
{
    class Program
    {
        static void Main(string[] args)
        {
            var number1 = new ComplexNumber()
            {
                RealPart = 100,
                ImaginaryPart = 10
            };

            var number2 = new ComplexNumber()
            {
                RealPart = 200,
                ImaginaryPart = -20
            };

            // TODO: overload for operator: true, false
            // TODO: overload for operator: ++
            Console.WriteLine("Before operation");
            Console.WriteLine($"number1 realpart = {number1.RealPart}\nnumber1 imaginarypart = {number1.ImaginaryPart}");
            Console.WriteLine($"number2 realpart = {number2.RealPart}\nnumber2 imaginarypart = {number2.ImaginaryPart}");
            ++number1;
            --number2;
            Console.WriteLine(new string('-', 50));
            Console.WriteLine("After operation ++number1 and --number2");
            Console.WriteLine($"number1 realpart = {number1.RealPart}\nnumber1 imaginarypart = {number1.ImaginaryPart}");
            Console.WriteLine($"number2 realpart = {number2.RealPart}\nnumber2 imaginarypart = {number2.ImaginaryPart}");
            Console.WriteLine(new string('-', 50));
            Console.WriteLine("true operator for number1");
            if (number1)
            {
                Console.WriteLine("number1.ImaginaryPart > 0 = True");
            }
            //Console.WriteLine("false operator for number2");
            if (number2)
            {
                Console.WriteLine("number2.ImaginaryPart < 0 = True");
            }
            Console.ReadLine();

        }
    }
}
