﻿using System;
using System.Threading;

namespace CSharp_Net_module1_6_1_lab
{
    class Program
    {
        static void Main(string[] args)
        {
            //use Console.ForegroundColor = ConsoleColor.Gray for output
            Console.ForegroundColor = ConsoleColor.Gray;
            //create ThreadManipulator object
            var manipulator = new ThreadManipulator();
            //create first thread for AddingOne method
            var del4thread1 = new ParameterizedThreadStart(manipulator.AddingOne);
            var thread1 = new Thread(del4thread1);
            //create second thread for AddingOne method
            var del4thread2 = new ParameterizedThreadStart(manipulator.AddingOne);
            var thread2 = new Thread(del4thread2);
            //create thread for AddingCustomValue method
            var del4thread3 = new ParameterizedThreadStart(manipulator.AddingCustomValue);
            var thread3 = new Thread(del4thread3);
            //create Background thread for Stop method
            var thread4 = new Thread(manipulator.Stop) { IsBackground = true };
            //start Background thread for Stop method
            thread4.Start();
            thread4.Join();
            //start first thread for AddingOne method with argument = 10
            thread1.Start(10);
            thread1.Join();
            //start second thread for AddingOne method with argument = 20
            thread2.Start(20);
            thread2.Join();
            //start thread for AddingCustomValue method with argument new[] { 5, 15 }
            thread3.Start(new[] { 5, 15 });
            //join threads
            thread3.Join();
            Console.ReadKey();

        }
    }
}
