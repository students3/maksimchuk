﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;

namespace Serialization
{
    class Program
    {
        static void Main(string[] args)
        {
            Rolle rolle1 = new Rolle
            {
                RolleId = 1,
                RolleName = "admin"
            };
            Rolle rolle2 = new Rolle
            {
                RolleId = 2,
                RolleName = "user"
            };
            Rolle rolle3 = new Rolle
            {
                RolleId = 3,
                RolleName = "moderator"
            };
            Rolle rolle4 = new Rolle
            {
                RolleId = 4,
                RolleName = "otheruser"
            };

            List<Rolle> rolleList = new List<Rolle>(new Rolle[] { rolle1, rolle2, rolle3, rolle4 });
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(rolleList);

            using(var writer = new StreamWriter("RolleList.json", true))
            {
                writer.WriteLine(json);
            }

            var rolleListFromFile = JsonConvert.DeserializeObject<List<Rolle>>(json);
            foreach (var item in rolleListFromFile)
            {
                Console.WriteLine("RoleId : "+item.RolleId+" RolleName : "+item.RolleName);
            }
            Console.WriteLine("finished");
            Console.ReadLine();
        }
    }
}
