﻿using System;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace LabWork_2._1
{
    class LibraryUser : ILibraryUser
    {
        // 1) declare interface ILibraryUser
        // declare method's signature for methods of class LibraryUser
        // 2) declare class LibraryUser, it implements ILibraryUser
        // 3) declare properties: FirstName (read only), LastName (read only),
        // Id (read only), Phone (get and set), BookLimit (read only)
        public string FirstName { get; }
        public string LastName { get; }
        public string BookLimit { get; }
        public string Phone { get; set; }
        public int Id { get; }

        // 4) declare field (bookList) as a string array
        private string[] bookList;

        // 5) declare indexer BookList for array bookList
        public string this[int i]
        {
            get { return bookList[i]; }
            set { bookList[i] = value; }
        }

        // 6) declare constructors: default and parameter
        public LibraryUser(string firstName, string lastName, string phone, int bookLimit, int id)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Phone = phone;
            this.bookList = new string[bookLimit];
            this.Id = id;

            for (int i = 0; i < BooksCount(); i++)
            {
                this[i] = string.Empty;
            }
        }

        public LibraryUser()
        {
            this.FirstName = "FirstName";
            this.LastName = "LastName";
            this.Phone = "+3 XXX XXX XX XX";
            this.bookList = new string[1];

            for (int i = 0; i < BooksCount(); i++)
            {
                this[i] = string.Empty;
            }
        }

        // 7) declare methods:
        //AddBook() – add new book to array bookList
        public void AddBook(string bookName)
        {
            bool addResult = false;

            //for (int i = 0, length = BooksCount() - 1; i < length; i++)
            //{
            //    // TODO: add book with resizing array !!! (Done)
            //    if (this[i].Equals(null) && this[i].Equals(String.Empty))
            //    {
            //        this[i] = bookName;
            //        addResult = true;
            //    }
            //}
            if (!string.IsNullOrEmpty(bookName))
            {
                Array.Resize(ref bookList, bookList.Length + 1);
                this[BooksCount() - 1] = bookName;
                addResult = true;
            }
            string addBookResult = addResult ? bookName + "added" : "book limit";
            Console.WriteLine();
        }

        //RemoveBook() – remove book from array bookList
        public void RemoveBook(string bookName)
        {
            bool searchResult = false;
            // TODO: remove book from array correctly. You can use Linq here if you want (Done)
            // .Where(x => !string.Equals(x, bookName)).ToArray()
                searchResult = bookList.Contains(bookName);
                if (searchResult)
                {
                    // this[i] = string.Empty;
                    bookList = bookList.Where(x => !string.Equals(x, bookName)).ToArray();
                }
            string removeResult = searchResult ? bookName + " deleted" : bookName + " not found";
            Console.WriteLine(removeResult);
        }

        //BookInfo() – returns book info by index
        public string BookInfo(int index)
        {
            return this[index];
        }

        //BooksCount() – returns current count of books
        public int BooksCount()
        {
            return bookList.Count();
        }
    }
}
