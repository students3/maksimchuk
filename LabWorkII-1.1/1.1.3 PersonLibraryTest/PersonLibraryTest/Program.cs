﻿using System;
using PersonLibrary;

namespace PersonLibraryTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Person student = new Person
            {
                FirstName = "Andy",
                LastName = "Sharp",
                Birthday = new DateTime(1995, 3, 15)
            };
            Console.WriteLine("Student info: " + student.FirstName + " " + student.LastName + ", birthday " + student.Birthday.ToString("dddd, d MMM, yyyy at HH:mm"));
            Console.ReadLine();
        }
    }
}
