﻿using System;

namespace PersonLibrary
{
    public class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Birthday { get; set; }
        public long Id { get; set; }
    }
}
