﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.IO;
using System.Linq;

namespace LabWork_5._5
{
    class Program
    {
        static void Main(string[] args)
        {
            var init = new CreateDatabaseIfNotExists<ProductDBContext>();
            using (var OnlineShopContext = new ProductDBContext("LabWork_5-5"))
            {
                //init.InitializeDatabase(OnlineShopContext);
                DB_init(OnlineShopContext);
                // Use transaction for operations, where you need to change data in several tables
                using (var transactTest = OnlineShopContext.Database.BeginTransaction())
                {
                    try
                    {
                        //var customerJohnId = int.Parse(Guid.NewGuid().ToString().Remove(4));
                        var customerJohn = new Customer
                        {
                            CustomerId = 1,
                            CustomerPhone = "566-432-532",
                            CustomerName = "John",
                            CustomerEmail = "john@gmail.com",
                            CustomerDetails = " some details"
                        };
                        //var customerAlexId = int.Parse(Guid.NewGuid().ToString().Remove(4));
                        var customerAlex = new Customer
                        {
                            CustomerId = 2,
                            CustomerPhone = "976-323-332",
                            CustomerName = "Alex",
                            CustomerEmail = "Alex@gmail.com",
                            CustomerDetails = "some details"
                        };
                        var orderByJohn = new Order
                        {
                            OrderId = 1,
                            CustomerId = 1,
                            OrderDate = DateTime.Parse("2017-11-17"),
                            OrderDetails = "John order details",
                            OrderStatus = "Complete"
                        };
                        var orderByAlex = new Order
                        {
                            OrderId = 2,
                            CustomerId = 2,
                            OrderDate = DateTime.Parse("2017-11-12"),
                            OrderDetails = "Alex order details",
                            OrderStatus = "TDB"
                        };
                        var samsungPhone = new Product
                        {
                            ProductId = 1,
                            ProductName = "Mobilephone",
                            ProductPrice = 700,
                            ProductType = "Phones",
                            SupplierId = 1,
                        };
                        var acesLaptop = new Product
                        {
                            ProductId = 2,
                            ProductName = "Laptop",
                            ProductPrice = 1000,
                            ProductType = "Notebooks",
                            SupplierId = 2,
                        };
                        var orderListPhones = new OrderList
                        {
                            OrderId = 1,
                            ProductId = 1,
                            ProductQuantity = 1
                        };
                        var orderListLaptops = new OrderList
                        {
                            OrderId = 2,
                            ProductId = 2,
                            ProductQuantity = 1
                        };
                        var supplierSamsung = new Supplier
                        {
                            SupplierId = 1,
                            SupplierEmail = "samsung@gmail.com",
                            SupplierName = "Samsung",
                            SupplierPhone = "999-323-332",
                            Products = new List<Product> { samsungPhone }
                        };
                        var supplierAcer = new Supplier
                        {
                            SupplierId = 2,
                            SupplierEmail = "acer@gmail.com",
                            SupplierName = "Acer",
                            SupplierPhone = "222-222-332",
                            Products = new List<Product> { acesLaptop }
                        };
                        // 9) Add data to database
                        // use method Add() of every element of context for instances with data
                        // for example, context.Products.Add(pr);
                        // where 'pr' - instance of class product
                        OnlineShopContext.Customers.Add(customerJohn);
                        OnlineShopContext.Customers.Add(customerAlex);
                        OnlineShopContext.Orders.Add(orderByJohn);
                        OnlineShopContext.Orders.Add(orderByAlex);
                        OnlineShopContext.OrdersList.Add(orderListLaptops);
                        OnlineShopContext.OrdersList.Add(orderListPhones);
                        OnlineShopContext.Products.Add(samsungPhone);
                        OnlineShopContext.Products.Add(acesLaptop);
                        OnlineShopContext.Suppliers.Add(supplierSamsung);
                        OnlineShopContext.Suppliers.Add(supplierAcer);
                        OnlineShopContext.SaveChanges();
                        transactTest.Commit();
                        OnlineShopContext.Dispose();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        Console.ReadLine();
                        transactTest.Rollback();
                    }

                    // Advanced:
                    // 10) export data to XML and JSON
                    //// 11) implort data from XML and JSON
                    using (SqlConnection connection = new SqlConnection(@"Data Source=(local);Initial Catalog=LabWork_5-5;Encrypt=False;Integrated Security=True"))
                    {
                        using (SqlCommand command = new SqlCommand("SELECT * FROM dbo.Products", connection))
                        {
                            var adapter = new SqlDataAdapter(command);
                            var dataSet = new DataSet();
                            adapter.Fill(dataSet, "Products");
                            
                            dataSet.WriteXml("Products.xml");
                            dataSet.WriteXmlSchema("ProductsSchema.xsd");
                            dataSet.Clear();
                            dataSet.ReadXml("Products.xml");
                            var dataTable = dataSet.Tables["Products"];
                            var row = dataTable.Rows[1];
                            //var columnsArray = new object[row.ItemArray.Length];
                            foreach (DataRow item in dataTable.Rows)
                            {
                                var msg = string.Join(" | ", item.ItemArray);
                                Console.WriteLine(msg);
                            }
                            command.Dispose();
                            adapter.Dispose();
                            connection.Dispose();
                        }
                    }
                    // NewtonJSON
                    //var filePath = Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), "OrderList.json");
                    //var orderList = OnlineShopContext.OrdersList;
                    //var jsonConv = Newtonsoft.Json.JsonConvert.SerializeObject(orderList);
                    //using (var fileWriter = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None))
                    //{
                    //    using (var streamWriter = new StreamWriter(fileWriter))
                    //    {
                    //        streamWriter.WriteLine(jsonConv);
                    //    }
                    //}
                    //Console.WriteLine("\nNewtonJson Deserialize:\n");
                    //var products = Newtonsoft.Json.JsonConvert.DeserializeObject(File.ReadAllText(filePath));
                    //if (products != null) Console.WriteLine(products.ToString());
                    //Console.ReadLine();

                }
            }
        }

        private static void DB_init(ProductDBContext ctx)
        {
            try
            {
                // Initialize database at this moment
                ctx.Database.Initialize(false);
            }
            catch (Exception)
            {
                Console.Write("Initialization error ?\r\n");
            }
        }
    }
}
