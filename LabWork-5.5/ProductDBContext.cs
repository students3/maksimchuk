﻿using System.Data.Entity;

namespace LabWork_5._5
{
    // 6) derive class ProductDBContext from DBContext
    class ProductDBContext : DbContext
    {

        public ProductDBContext(string connString) : base("LabWork_5-5")
        {

        }
        // 7) add constructor; use name of database as string parameter for base constructor invoking;
        // add to App.config file in <entityFramework>
        // <defaultConnectionFactory type=...
        // <parameters>
        // connection string:
        //  <parameter value="data source=MyServer; Integrated Security=True;"/>
        // where "MyServer" - name of database server

        // 8) declare generic DBSets for all classes of database; they must be auto-properties
        public DbSet<Supplier> Suppliers { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderList> OrdersList { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Product> Products { get; set; }
    }
}
