﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LabWork_5._5
{
    [Table ("Suppliers")]
    public class Supplier
    {
        // 5) Add properties: SuplierName, SuplierPhone, SupplierEmail
        [Key]
        public int SupplierId { get; set; }
        public string SupplierName { get; set; }
        public string SupplierPhone { get; set; }
        public string SupplierEmail { get; set; }
        public ICollection<Product> Products { get; set; }
    }
}
