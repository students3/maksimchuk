﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace LabWork_5._5
{
    [Table("Products")]
    public class Product
    {
        // 1) Add properties: ProductName, ProductPrice, ProdroductType, ProductPrice, SupplierId
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductType { get; set; }
        public decimal ProductPrice { get; set; }
        public int SupplierId { get; set; }
        public virtual Supplier Suppliers { get; set; }
        public ICollection<OrderList> OrderList { get; set; } 
}
}
