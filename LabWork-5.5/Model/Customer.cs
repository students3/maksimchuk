﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LabWork_5._5
{
    [Table("Customers")]
    public class Customer
    {
        // 2) Add properties: CustomerName, CustomerPhone, CustomerEmail, CustomerDetails
        [Key]
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerDetails { get; set; }
        public ICollection<Order> Orders { get; set; }
    }
}
