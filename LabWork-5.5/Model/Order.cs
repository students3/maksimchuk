﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LabWork_5._5
{
    [Table("Orders")]
    public class Order
    {
        // 3) Add properties: CustomerId, OrderStatus, OrderDetails, OrderDate
        [Key]
        public int OrderId { get; set; }
        public string OrderStatus { get; set; }
        public string OrderDetails { get; set; }
        public DateTime OrderDate { get; set; }
        public int CustomerId { get; set; }
        public virtual Customer Customer {get; set;}
        public ICollection<OrderList> OrderList { get; set; }
    }
}
