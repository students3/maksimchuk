﻿using System;
using System.Collections.Generic;
using System.IO;

namespace CSharp_Net_module1_7_1_lab
{
    class Program
    {
        //TODO: move validation of the path to the other public method and check path validity in program (In progress)
        // showing which symbols are not valid in the path

        // Directory with text file was not created on my PC



        static void Main(string[] args)
        {
            // 3) create collection of computers;
            var computerAcer = new Computer { Memory = 4, Cores = 4, Frequency = 3, Hdd = 1000 };
            var computerAsus = new Computer { Memory = 6, Cores = 6, Frequency = 4, Hdd = 2000 };
            var computerDell = new Computer { Memory = 8, Cores = 8, Frequency = 2, Hdd = 2500 };
            var computerSamsung = new Computer { Memory = 6, Cores = 4, Frequency = 3, Hdd = 3000 };

            var computerList = new List<Computer>(4) {computerAcer, computerAsus, computerDell, computerSamsung};
            var computerDb = new InOutOperation();
            var divider = new string('-', 50);
            var newFileName = string.Empty;
            var newDirectoryName = string.Empty;
            //var newFilePath = String.Empty;

            // set path to file and file name
            Console.WriteLine("Default Current Path:");
            Console.WriteLine(computerDb.CurrentPath);
            Console.WriteLine(divider);

            Console.WriteLine("Default Directory Name:");
            Console.WriteLine(computerDb.CurrentDirectory);
            Console.WriteLine(divider);

            Console.WriteLine("Default File Name:");
            Console.WriteLine(computerDb.CurrentFile);
            Console.WriteLine(divider);

            Console.WriteLine($"Set new file name:");
            Console.WriteLine($"Please enter filename w/o extension: ");
            newFileName = Console.ReadLine();
            computerDb.CurrentFile = newFileName;
            Console.WriteLine(divider);

            Console.WriteLine("New File Name:");
            Console.WriteLine(computerDb.CurrentFile);
            Console.WriteLine(divider);

            Console.WriteLine($"Set new directory name:");
            Console.WriteLine($"Please enter directory name: ");
            newDirectoryName = Console.ReadLine();
            computerDb.CurrentDirectory = newDirectoryName;
            Console.WriteLine(divider);

            Console.WriteLine("New Directory Name:");
            Console.WriteLine(computerDb.CurrentDirectory);
            Console.WriteLine(divider);

            Console.WriteLine($"Set new file path:");
            Console.WriteLine($"Please enter file path: ");
            computerDb.CurrentPath = Console.ReadLine();
            Console.WriteLine(divider);

            Console.WriteLine("New Path:");
            Console.WriteLine(computerDb.CurrentPath);
            Console.WriteLine(divider);

            string newLocationPath = Path.Combine(computerDb.CurrentPath, computerDb.CurrentDirectory, computerDb.CurrentFile);
            Console.WriteLine($"new location path:");
            Console.WriteLine(newLocationPath);
            Console.WriteLine(divider);
            if (computerDb.VaildatePath(newLocationPath))
            computerDb.ChangeLocation(newLocationPath);
            Console.WriteLine(divider);

            Console.WriteLine($"Write ComputerList to {computerDb.CurrentFile}");
            // 4) save data and read it in the seamplest way (with WriteData() and ReadData() methods)
            computerDb.WriteData(computerList);
            Console.WriteLine(divider);

            Console.WriteLine("Read from file : ");
            computerDb.ReadData();
            Console.WriteLine(divider);

            // 5) save data and read it with WriteZip() and ReadZip() methods
            Console.WriteLine($"Compress {computerDb.CurrentFile} to "+Path.GetFileNameWithoutExtension(computerDb.CurrentFile)+".zip");
            computerDb.WriteZip(computerList);
            Console.WriteLine(divider);

            Console.WriteLine("Read from "+ Path.GetFileNameWithoutExtension(computerDb.CurrentFile) + ".zip");
            computerDb.ReadZip();
            Console.WriteLine(divider);

            Console.WriteLine($"Move {newLocationPath} to D:\\");
            computerDb.ChangeLocation($"D:\\");
            string currentFullFilePath = Path.Combine(computerDb.CurrentPath, computerDb.CurrentDirectory, computerDb.CurrentFile);
            Console.WriteLine($"Current full path : {currentFullFilePath}");

            // 6) read info about computers asynchronously (from the 1st file)
            // While asynchronous method will be running, Main() method must print ‘*’

            // use
            // collection of Task with info about computers as type to get returned data from method ReadAsync()
            // use property Result of collection of Task to get access to info about computers

            // Note:
            // print all info about computers after reading from files


            // ADVANCED:
            // 8) save data to memory stream and from memory to file
            // declare file stream and set it to null
            // call method WriteToMemory() with info about computers as parameter
            // save returned stream to file stream

            // call method WriteToFileFromMemory() with parameter of file stream
            // open file with saved data and compare it with input info
            Console.ReadKey();
        }
    }
}
