﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.IO.Compression;
using System.Text;

namespace CSharp_Net_module1_7_1_lab
{
    class InOutOperation
    {
        private readonly char[] _forbiddenCharArrayForDirOrName = Path.GetInvalidFileNameChars();
        private string fileNameErrorMsg = "The file name can not contain the following characters : /, \\, *, :, ?, >, <, \", |";
        private string currentLocationPath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        private string currentFileName = "ComputersConfigurations.txt";
        private string currentDirName = new FileInfo(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location)).Name;

        // 1) declare properties  CurrentPath - path to file (without name of file), CurrentDirectory - name of current directory,
        // CurrentFile - name of current file
        #region Properties
        public string CurrentPath
        {
            get
            {
                return currentLocationPath;
            }
            set
            {
                var invalidCharSet = new HashSet<char>(Path.GetInvalidPathChars());
                var valueCharSet = new HashSet<char>(value.ToCharArray());
                var charIsEntryFlag = valueCharSet.Any(item => invalidCharSet.Contains(item));

                while (charIsEntryFlag)
                {
                    Console.WriteLine("Error: path contain Invalid Path Chars : " + value);
                    Console.WriteLine("Please enter new path again : ");
                    value = Console.ReadLine();
                }

                while (string.IsNullOrWhiteSpace(value))
                {
                    Console.WriteLine("ERROR: pathname incorrect");
                    Console.WriteLine("Please enter filename again : ");
                    value = Console.ReadLine();
                }

                while ((value.ToCharArray())[0] == '.')
                {
                    Console.WriteLine("ERROR: pathname should not begin with a dot");
                    Console.WriteLine("Please enter filename again : ");
                    value = Console.ReadLine();
                }

                while (Path.HasExtension(value))
                {
                    Console.WriteLine("Error : path can not contain extension");
                    Console.WriteLine("Please enter new path : ");
                    value = Console.ReadLine();
                }

                if (value.Contains('/'))
                {
                    value = value.Replace('/', Path.DirectorySeparatorChar);
                }
                currentLocationPath = value;
            }
        }

        public string CurrentDirectory
        {
            get
            {
                return currentDirName;
            }
            set
            {
                while (forbiddenCharCheck(value, _forbiddenCharArrayForDirOrName))
                {
                    Console.WriteLine(fileNameErrorMsg);
                    Console.WriteLine("Please enter directory name again : ");
                    value = Console.ReadLine();
                }
                while (string.IsNullOrWhiteSpace(value))
                {
                    Console.WriteLine("ERROR: directory name incorrect");
                    Console.WriteLine("Please enter directory name again : ");
                    value = Console.ReadLine();
                }
                while ((value.ToCharArray())[0] == '.')
                {
                    Console.WriteLine("ERROR: directory name should not begin with a dot");
                    Console.WriteLine("Please enter directory name again : ");
                    value = Console.ReadLine();
                }
                value = Path.GetFileNameWithoutExtension(value);
                while (value.Contains('.'))
                {
                    value = Path.GetFileNameWithoutExtension(value);
                }

                currentDirName = value;
            }
        }

        public string CurrentFile
        {
            get
            {
                return currentFileName;
            }
            set
            {
                while (forbiddenCharCheck(value, _forbiddenCharArrayForDirOrName))
                {
                    Console.WriteLine(fileNameErrorMsg);
                    Console.WriteLine("Please enter filename again : ");
                    value = Console.ReadLine();
                }
                while (string.IsNullOrWhiteSpace(value))
                {
                    Console.WriteLine("ERROR: filename incorrect");
                    Console.WriteLine("Please enter filename again : ");
                    value = Console.ReadLine();
                }
                while ((value.ToCharArray())[0] == '.')
                {
                    Console.WriteLine("ERROR: File name should not begin with a dot");
                    Console.WriteLine("Please enter filename again : ");
                    value = Console.ReadLine();
                }
                value = Path.GetFileNameWithoutExtension(value);
                while (value.Contains('.'))
                {
                    value = Path.GetFileNameWithoutExtension(value);
                }
                currentFileName = value + ".txt";
            }
        }
        #endregion

        #region Methods
        // check file path on forbidden characters
        private bool forbiddenCharCheck(string pathOrName, char[] invalidCharArray)
        {
            return invalidCharArray.Any(item => pathOrName.Contains(item));
        }

        // 2) declare public methods:
        //ChangeLocation() - change of CurrentPath;
        // method takes new file path as parameter, creates new directories (if it is necessary)
        public void ChangeLocation(string newPath)
        {
            currentDirName = String.Empty;
            currentLocationPath = Path.GetDirectoryName(newPath) ?? Path.GetPathRoot(newPath);
        }

        public bool VaildatePath(string newPath)
        {
            if (string.IsNullOrEmpty(newPath) || (newPath.IndexOfAny(Path.GetInvalidPathChars()) != -1))
                return false;
            try
            {
                var tempFileInfo = new FileInfo(newPath);
                return true;
            }
            catch (NotSupportedException)
            {
                Console.WriteLine("ERROR: Wrong Path !!!");
                return false;
            }
        }


        //public void ValidatePath(string newPath)
        //{
        //    var invalidCharSet = new HashSet<char>(Path.GetInvalidPathChars());
        //    var charIsEntryFlag = newPath.Any(item => invalidCharSet.Contains(item));


        //    //TODO: move validation of the path to the other public method and check path validity in program (Done)
        //    // showing which symbols are not valid in the path
        //    while (charIsEntryFlag)
        //    {
        //        Console.WriteLine("Error : path contain Invalid Path Chars : " + newPath);
        //        Console.WriteLine("Please enter new path again : ");
        //        newPath = Console.ReadLine();
        //    }

        //    while (string.IsNullOrWhiteSpace(newPath))
        //    {
        //        Console.WriteLine("ERROR: pathname incorrect");
        //        Console.WriteLine("Please enter filename again : ");
        //        newPath = Console.ReadLine();
        //    }

        //    while ((newPath.ToCharArray())[0] == '.')
        //    {
        //        Console.WriteLine("ERROR: pathname should not begin with a dot");
        //        Console.WriteLine("Please enter filename again : ");
        //        newPath = Console.ReadLine();
        //    }

        //    if (Path.HasExtension(newPath))
        //    {
        //        if (File.Exists(newPath))
        //        {
        //            Console.WriteLine($"{newPath} exists !");
        //        }
        //        else
        //        {
        //            string pathWithoutFile = Path.GetDirectoryName(newPath);
        //            Directory.CreateDirectory(pathWithoutFile);
        //            using (File.Create(newPath)) ;
        //            Console.WriteLine($"file {Path.GetFileName(newPath)} is created by the full path : {newPath}");
        //            return;
        //        }
        //    }
        //    else
        //    {
        //        string sourceFileName = Path.Combine(CurrentPath, CurrentDirectory, CurrentFile);
        //        string destFileName = Path.Combine(newPath, currentFileName);
        //        File.Move(sourceFileName, destFileName);
        //    }
        //}

        // CreateDirectory() - create new directory in current location
        public void CreateDirectory(string dirName)
        {

            if (this.forbiddenCharCheck(dirName, _forbiddenCharArrayForDirOrName))
            {
                Console.WriteLine(fileNameErrorMsg);
            }
            string newDirectoryPath = Path.Combine(currentLocationPath, dirName);
            if (Directory.Exists(newDirectoryPath))
            {
                Console.WriteLine("Directory not created");
                Console.WriteLine(newDirectoryPath + " already exists");
                return;
            }
            else
            {
                Directory.CreateDirectory(newDirectoryPath);
                Console.WriteLine($"Directory {dirName} created");
            }
        }
        // WriteData() – save data to file
        // method takes data (info about computers) as parameter
        public void WriteData(List<Computer> computerList)
        {
            string fullPathToFile = Path.Combine(CurrentPath, CurrentDirectory, CurrentFile);
            string rowOfcomputerFields = String.Empty;

            if (computerList == null)
            {
                Console.WriteLine("List of computers is NULL");
                return;
            }
            if (computerList.Count == 0)
            {
                Console.WriteLine("List of computers is empty");
                Console.WriteLine("Missing data for writing");
            }
            else
            {
                if (File.Exists(fullPathToFile))
                {
                    using (var writer = new StreamWriter(fullPathToFile, true))
                    {
                        foreach (var item in computerList)
                        {
                            rowOfcomputerFields = Path.Combine(item.GuidString, item.Cores.ToString(),
                            item.Frequency.ToString(), item.Hdd.ToString(), item.Memory.ToString()).Replace('\\', '|');
                            writer.WriteLine(rowOfcomputerFields);
                        }
                        writer.Close();
                    }
                }
                else
                {
                    using (var fileStream = new FileStream(fullPathToFile, FileMode.OpenOrCreate))
                    {
                        using (var writer = new StreamWriter(fileStream))
                        {
                            foreach (var item in computerList)
                            {
                                rowOfcomputerFields = Path.Combine(item.GuidString, item.Cores.ToString(),
                                item.Frequency.ToString(), item.Hdd.ToString(), item.Memory.ToString()).Replace('\\', ',');
                                writer.WriteLine(rowOfcomputerFields);
                            }
                        }
                    }
                }
            }
        }
        // ReadData() – read data from file
        // method returns info about computers after reading it from file
        public void ReadData()
        {
            string fullPathToFile = Path.Combine(CurrentPath, CurrentDirectory, CurrentFile);
            if (File.Exists(fullPathToFile))
            {
                IEnumerable<string> reader = File.ReadLines(fullPathToFile);
                foreach (var line in reader)
                {
                    Console.WriteLine(line);
                }
            }
        }
        // WriteZip() – save data to zip file
        // method takes data (info about computers) as parameter
        public void WriteZip(List<Computer> computerList)
        {
            string fileFullPath = Path.ChangeExtension((Path.Combine(CurrentPath, CurrentDirectory, CurrentFile)), ".zip");
            FileStream source = File.Create(fileFullPath);
            GZipStream compressor = new GZipStream(source, CompressionMode.Compress);
            string rowOfcomputerFields = string.Empty;
            foreach (var item in computerList)
            {
                rowOfcomputerFields += Path.Combine(item.GuidString, item.Cores.ToString(),
                item.Frequency.ToString(), item.Hdd.ToString(), item.Memory.ToString()).Replace('\\', '|') + "\r\n";
            }
            var byteArray = Encoding.Unicode.GetBytes(rowOfcomputerFields);
            Console.WriteLine("information: ");
            var stringsFromFile = Encoding.Default.GetString(byteArray).Replace("\0", String.Empty);
            Console.Write(stringsFromFile);
            Console.WriteLine($"is compressed into the archive {fileFullPath}");
            compressor.Write(byteArray, 0, byteArray.Length);
            compressor.Close();
            source.Close();
        }
        // ReadZip() – read data from file
        // method returns info about computers after reading it from file
        public void ReadZip()
        {

            string fullFilePath = Path.Combine(CurrentPath, CurrentDirectory, CurrentFile);
            string uncompressedFilePath = fullFilePath.Replace(Path.GetFileName(fullFilePath), "uncompressed_" + Path.GetFileName(fullFilePath));
            FileStream source = File.OpenRead(Path.ChangeExtension(fullFilePath, ".zip"));
            FileStream destination = File.Create(uncompressedFilePath);
            GZipStream decompressor = new GZipStream(source, CompressionMode.Decompress);
            int onByte = decompressor.ReadByte();
            Console.WriteLine("Information : ");
            while (decompressor.ReadByte() != -1)
            {
                destination.WriteByte((byte)onByte);
                Console.Write(Encoding.Default.GetString(new byte[] { (byte)onByte }));
                onByte = (byte)decompressor.ReadByte();
            }
            Console.WriteLine("extracted from the archive into a file "+uncompressedFilePath);
            decompressor.Close();
            source.Close();
        }
        #endregion
        // ReadAsync() – read data from file asynchronously
        // method is async
        // method returns Task with info about computers
        // use ReadLineAsync() method to read data from file
        // Note: don't forget about await

        // 7)
        // ADVANCED:
        // WriteToMemoryStream() – save data to memory stream
        // method takes data (info about computers) as parameter
        // info about computers is saved to Memory Stream

        // use  method GetBytes() from class UnicodeEncoding to save array of bytes from string data
        // create new file stream
        // use method WriteTo() to save memory stream to file stream
        // method returns file stream

        // WriteToFileFromMemoryStream() – save data to file from memory stream and read it
        // method takes file stream as parameter
        // save data to file


        // Note:
        // - use '//' in path string or @ before it (for correct path handling without escape sequence)
        // - use 'using' keyword to organize correct working with files
        // - don't forget to check path before any file or directory operations
        // - don't forget to check existed directory and file before creating
        // use File class to check files, Directory class to check directories, Path to check path

    }
}

