﻿using System;

namespace CSharp_Net_module1_7_1_lab
{
    class Computer
    {
        public string GuidString
        {
            get
            {
                return Guid.NewGuid().ToString("N");
            }
        }
        public int Cores { get; set; }
        public double Frequency { get; set; }
        public int Memory { get; set; }
        public int Hdd { get; set; }
    }
}
