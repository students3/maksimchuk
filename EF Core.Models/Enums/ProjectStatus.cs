﻿namespace EF_Core.Models.Enums
{
    public enum ProjectStatus
    {
        [StringValue("Traded")]
        TR,
        [StringValue("Traded")]
        OF,
        [StringValue("Traded")]
        IS
    }
}
