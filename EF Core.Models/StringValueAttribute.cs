﻿using System;

namespace EF_Core.Models
{
    public class StringValueAttribute : Attribute
    {
        private string _value;

        public StringValueAttribute(string value)
        {
            _value = value;
        }
    }
}
