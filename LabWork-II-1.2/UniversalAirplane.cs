﻿using System;
using AirplaneLibrary;
using ClassLibrary1;

namespace LabWork_II_1._2
{
    [AirplaneTypeAttribute(AirplaneTypesEnum.CargoPlane)]
    [AirplaneTypeAttribute(AirplaneTypesEnum.Jet)]
    public class UniversalAirplane
    {
        public UniversalAirplane()
        {
            Console.WriteLine("UniversalAirplane ctor");
        }
    }
}
