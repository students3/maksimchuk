﻿using System;
using AirplaneLibrary;
using ClassLibrary1;


namespace LabWork_II_1._2
{
    [AirplaneTypeAttribute(AirplaneTypesEnum.CargoPlane)]
    public class CargoAirplane
    {
        public CargoAirplane()
        {
            Console.WriteLine("CargoAirplane ctor");
        }
    }
}
