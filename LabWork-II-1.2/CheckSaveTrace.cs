﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using AirplaneLibrary;
using LabWork_II_1._2;

namespace CSharp_Net_module1_5_2_lab
{
    //immplement class CheckSaveTrace:IDisposable
    public class CheckSaveTrace : IDisposable
    {
        //define  TraceSource and EventLog  fields
        public TraceSource TSource { get; set; }

        public EventLog ELog { get; set; }

        //implement CheckSaveTrace() constructor with two methods calling
        //    initEventLog with assembly name as argument
        //    initTrace with assembly name as argument
        public CheckSaveTrace(Assembly asm)
        {
            initEventLog(asm.FullName);
            initTrace(asm.FullName);
        }

        //implement Dispose() method
        public void Dispose()
        {
            ELog.Dispose();
        }

        //implement ~CheckSaveTrace()
        ~CheckSaveTrace()
        {
            Dispose();
        }

        //implement initEventLog method with assembly name as argument
        public void initEventLog(string assemblyName)
        {

        }

        //implement   initTrace  method with assembly name as argument
        public void initTrace(string assemblyName)
        {
            TSource = new TraceSource("TraceTest");
            Console.WriteLine("TraceSource name = " + TSource.Name);
            Console.WriteLine("TraceSource switch level = " + TSource.Switch.Level);
            Console.WriteLine("TraceSource switch = " + TSource.Switch.DisplayName);
            SwitchAttribute[] switches = SwitchAttribute.GetAll(typeof(CheckSaveTrace).Assembly);
            for (int i = 0; i < switches.Length; i++)
            {
                Console.WriteLine("Switch name = " + switches[i].SwitchName);
                Console.WriteLine("Switch type = " + switches[i].SwitchType);
            }
            TSource.Flush();
            TSource.Close();
        }

        //implement  CheckClassAttribute(object obj) method
        //for  attributes output
        public void CheckClassAttribute(object obj)
        {
            Type type = obj.GetType();
            object[] attributes = null;

            AirplaneTypeAttribute attribute = null;
            attributes = type.GetCustomAttributes(false);

            foreach (object attributeType in attributes)
            {
                attribute = attributeType as AirplaneTypeAttribute;
                Console.WriteLine("Attribute Analyze  : Type = {0}, TypeId = {1}", attribute.Type, attribute.TypeId);
            }

        }

        //implement SaveTrace(object obj) method
        //for obj attributes tracing
        public void SaveTrace(object obj)
        {
            var traceFile = File.Create("TraceExample.txt");
            var txtListener = new TextWriterTraceListener(traceFile);
            TSource = new TraceSource("TraceSource", SourceLevels.All);
            TSource.Listeners.Clear();
            TSource.Listeners.Add(txtListener);
            TSource.TraceInformation("Trace output");
            TSource.Flush();
            TSource.Close();
        }

        //implement EventLogging(object obj) method
        //for obj attributes logging
        public void EventLogging(object obj)
        {
            if (!EventLog.SourceExists("LogSource"))
            {
                EventLog.CreateEventSource("LogSource","Debug_TraceV_Log");
                Console.WriteLine("Created_EvenSource");
                Console.WriteLine("Please press any key");
                Console.ReadKey();
            }
            ELog = new EventLog();
            ELog.Source = "LogSource";
            ELog.WriteEntry("Writing to event log.");
            Type type = obj.GetType();
            object[] attributes = null;

            AirplaneTypeAttribute attribute = null;
            attributes = type.GetCustomAttributes(false);

            foreach (object attributeType in attributes)
            {
                attribute = attributeType as AirplaneTypeAttribute;
                ELog.WriteEntry($"Attribute Analyze  : Type = {attribute.Type}, TypeId = {attribute.TypeId}");
            }
        }
    }
}
