﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AirplaneLibrary;
using LabWork_II_1._2;

namespace CSharp_Net_module1_5_2_lab
{
    class Program
    {
        static void Main(string[] args)
        {
            //implement  CheckSaveTrace in using block
            {
                //create the  CargoAirplane and UniversalAirplane objects
                var cargoAirplaneInstance = new CargoAirplane();
                var universalAirplaneInstance = new UniversalAirplane();
                var checkAttributeInstance = new CheckSaveTrace(System.Reflection.Assembly.GetExecutingAssembly());
                //call CheckClassAttribute, SaveTrace and EventLogging methods
                //for the  CargoAirplane and UniversalAirplane objects
                checkAttributeInstance.CheckClassAttribute(cargoAirplaneInstance);
                checkAttributeInstance.CheckClassAttribute(universalAirplaneInstance);
                Console.WriteLine(new string('-',50));
                //checkAttributeInstance.SaveTrace(cargoAirplaneInstance);
                //checkAttributeInstance.SaveTrace(universalAirplaneInstance);
                //checkAttributeInstance.EventLogging(cargoAirplaneInstance);
                //checkAttributeInstance.EventLogging(universalAirplaneInstance);

            }
            Console.ReadKey();
        }
    }
}
