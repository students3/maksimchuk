﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabWork_2._4
{
    //Implement class Morse_matrix derived from String_matrix, which realize IMorse_crypt
    public class Morse_matrix : String_matrix, IMorse_crypt
    {
        public const int Size_2 = Alphabet.Size;
        private int offset_key = 0;

        //Implement Morse_matrix constructor with the int parameter for offset
        //Use fd(Alphabet.Dictionary_arr) and sd() methods
        public Morse_matrix(int offset = 0)
        {
            offset_key = offset;
        }
        //Implement Morse_matrix constructor with the string [,] Dict_arr and int parameter for offset
        //Use fd(Dict_arr) and sd() methods
        public Morse_matrix(string[,] Dict_arr, int offset = 0)
        {
            offset_key = offset;
        }
        private void fd(string[,] Dict_arr)
        {
            for (int ii = 0; ii < Size1; ii++)
                for (int jj = 0; jj < Size_2; jj++)
                    this[ii, jj] = Dict_arr[ii, jj];
        }


        private void sd()
        {
            int off = Size_2 - offset_key;
            for (int jj = 0; jj < off; jj++)
                this[1, jj] = this[1, jj + offset_key];
            for (int jj = off; jj < Size_2; jj++)
                this[1, jj] = this[1, jj - off];
        }

        //Implement Morse_matrix operator +
        //Realize crypt() with string parameter
        public void Crypt(string stringForCrypt)
        {
            if (!string.IsNullOrEmpty(stringForCrypt))
            {
                char[] charArray = stringForCrypt.ToCharArray();
                var MorseDict = Alphabet.Dictionary_arr;
                var sb = new StringBuilder();
                foreach (var item in charArray)
                {
                    string searchElement = char.ToString(item);
                    for (int j = 0; j < 36; j++)
                    {
                        string elementOfArray = MorseDict[0, j];
                        if (elementOfArray.Equals(searchElement))
                        {
                            sb.Append(MorseDict[1, j]);
                        }
                    }
                }
                Console.WriteLine(sb.ToString());
            }
        }

        //Use indexers
        //Realize decrypt() with string array parameter
        public void Decrypt(string[] arr)
        {
            var MorseDict = Alphabet.Dictionary_arr;
            StringBuilder sb = new StringBuilder();
            foreach(var item in arr)
            {
                for (int i = 0; i < 36; i++)
                {
                    var element = MorseDict[1, i];
                    if ((element).Equals(item))
                    {
                        sb.Append(MorseDict[0, i]);
                    }
                }
            }
            Console.WriteLine(sb);
        }
        public string DecryptString(string[] arr)
        {
            var MorseDict = Alphabet.Dictionary_arr;
            StringBuilder sb = new StringBuilder();
            foreach (var item in arr)
            {
                for (int i = 0; i < 36; i++)
                {
                    var element = MorseDict[1, i];
                    if ((element).Equals(item))
                    {
                        sb.Append(MorseDict[0, i]);
                    }
                }
            }
            return sb.ToString();
        }
        //Use indexers
        //Implement Res_beep() method with string parameter to beep the string
    }
}
