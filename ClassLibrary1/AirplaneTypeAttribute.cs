﻿using ClassLibrary1;
using System;

namespace AirplaneLibrary
{
    // 1) add enum AirplaneTypes with values SportPlane, CargoPlane, TurboProp, Jet

    // 2) set attribute AttributeUsage with parameters that allow to use class AirplaneTypeAttribute
    // for classes only, disable inheritance and enable multiple using

    // 3) derive class AirplaneTypeAttribute from System.Attribute and protect against inheritance
    [AttributeUsage(AttributeTargets.Class,AllowMultiple = true,Inherited = false)]
    public class AirplaneTypeAttribute : System.Attribute
    {

        // 4) declare public property Type with type - AirplaneTypes
        public AirplaneTypesEnum Type { get; set; }
        // 5) declare 2 constructors:
        // default - initialize Type  with AirplaneTypes.TurboProp
        // parameter - with type AirplaneTypes
        public AirplaneTypeAttribute()
        {
            this.Type = AirplaneTypesEnum.TurboProp;
        }
        public AirplaneTypeAttribute(AirplaneTypesEnum type)
        {
            this.Type = type;
        }

        //add two AirplaneTypeAttribute-s to UniversalAirplane

        //add AirplaneTypeAttribute to CargoAirplane

    }
}
