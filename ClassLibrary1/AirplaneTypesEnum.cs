﻿namespace ClassLibrary1
{
    public enum AirplaneTypesEnum
    {
        SportPlane,
        CargoPlane,
        TurboProp,
        Jet
    }
}
