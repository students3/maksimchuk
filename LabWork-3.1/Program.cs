﻿using System;

namespace LabWork_3._1
{
    class Program
    {
        static void Main(string[] args)
        {
            CatchExceptionClass cec = new CatchExceptionClass();
            cec.CatchExceptionMethod();
            // 11) Make some unhandled exception and study Visual Studio debugger report –
            // read description and find the reason of exception
            try
            {
                Action StackOverflowInit = null;
                StackOverflowInit = () => {
                    Console.WriteLine("Bit more !!!");
                    StackOverflowInit.Invoke();
                };
                StackOverflowInit.Invoke();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                Console.WriteLine("!!! Finally-block never works (: !!!");
            }
            Console.ReadLine();
        }
    }
}
