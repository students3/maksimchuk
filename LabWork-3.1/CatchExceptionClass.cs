﻿using System;

namespace LabWork_3._1
{
    class CatchExceptionClass
    {
        public void CatchExceptionMethod()
        {
            try
            {
                MyArray ma = new MyArray();
                // 3) replace second elevent of array by 0
                int[] arr = new int[4] { 1, 4, 8, 5 };
                arr[1] = 0;
                ma.Assign(arr, 4);
            }
            // 8) catch all other exceptions here
            catch(Exception e)
            {
                // 9) print System.Exception properties:
                // HelpLink, Message, Source, StackTrace, TargetSite
                Console.WriteLine($"HelpLink = {e.HelpLink}");
                Console.WriteLine($"Message = {e.Message}");
                Console.WriteLine($"Source = {e.Source}");
                Console.WriteLine($"StackTrace = {e.StackTrace}");
                Console.WriteLine($"TargetSite = {e.TargetSite}");
            }
            // 10) add finally block, print some message
            // explain features of block finally
            finally
            {
                Console.WriteLine("block finally is always executed");
            }
        }
    }
}
