﻿using System;

namespace LabWork_3._1
{
    class MyArray
    {
        int[] arr;
        public void Assign(int[] arr, int size)
        {
            // 5) add block try (outside of existing block try)
            try
            {
                try
                {
                    this.arr = new int[size];
                    // 1) assign some value to cell of array arr, which index is out of range
                    for (int i = 0; i < arr.Length; i++)
                        this.arr[i] = arr[i] / arr[i + 1];
                    this.arr[arr.Length + 1] = 9999;
                    // 7) use unchecked to assign result of operation 1000000000 * 100
                    // to last cell of array
                    int x = 1000000000;
                    int y = 100;
                    this.arr[arr.Length - 1] = x * y;
                    //NullReferenceException
                    throw new NullReferenceException("take NullReferenceException");
                }
                // 2) catch exception index out of rage
                catch (IndexOutOfRangeException e)
                {
                    // output message
                    Console.WriteLine("Went beyond the bounds of the array");
                    Console.WriteLine(e.Message);
                }
            }
            // 4) catch devision by 0 exception
            catch (DivideByZeroException e)
            {
                // output message
                Console.WriteLine("division by zero is forbidden");
                Console.WriteLine(e.Message);
            }
            // 6) add catch block for null reference exception of outside block try
            // change the code to execute this block (any method of any class)
            catch (NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
