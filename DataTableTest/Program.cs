﻿using System.Data;

namespace DataTableTest
{
    class Program
    {
        static void Main(string[] args)
        {
            GetDataSet();
        }
        private static DataTable GetHistoryDataTable()
        {
            var dtInput = new DataTable
            {
                TableName = "History"
            };
            //TODO:
            dtInput.Columns.Add("Letter", typeof(System.String));
            dtInput.Columns.Add("OriginalColumn", typeof(System.String));
            dtInput.Columns.Add("MapTo", typeof(System.String));
            var dr = dtInput.NewRow();
            dr[0] = "Letter";
            dr[1] = "Task";
            dr[2] = "TaskName";
            dtInput.Rows.Add(dr);
            dr = dtInput.NewRow();
            dr[0] = "Letter";
            dr[1] = "Id";
            dr[2] = "TaskId";
            dtInput.Rows.Add(dr);
            return dtInput;
        }
        private static DataTable GetMappingDataTable()
        {
            //TODO:
            var dtInput = new DataTable
            {
                TableName = "Mapping"
            };
            dtInput.Columns.Add("Letter", typeof(System.String));
            dtInput.Columns.Add("OriginalColumn", typeof(System.String));
            dtInput.Columns.Add("MapTo", typeof(System.String));
            var dr = dtInput.NewRow();
            dr[0] = "Letter";
            dr[1] = "Task";
            dr[2] = "TaskName";
            dtInput.Rows.Add(dr);
            dr = dtInput.NewRow();
            dr[0] = "Letter";
            dr[1] = "Id";
            dr[2] = "TaskId";
            dtInput.Rows.Add(dr);
            return dtInput;
        }
        private static void GetDataSet()
        {
            var ds = new DataSet();
            ds.Tables.Add(GetMappingDataTable());
            ds.Tables.Add(GetHistoryDataTable());
        }
        private static void GetDataSetFromDb()
        {

        }
    }
}
