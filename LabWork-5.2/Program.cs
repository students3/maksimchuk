﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using Newtonsoft.Json.Converters;

namespace LabWork_5._2
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create instance of Student class
            // Initialize its properties
            // Call methods for serialization and deserialization
            Student studentVasya = new Student();
            studentVasya.FirstName = "Vasya";
            studentVasya.LastName = "Pupkin";
            studentVasya.Nationality = "Ukrainian";
            studentVasya.SetAddress("Nikolaev", "054");
            // Implement BinaryFormatter object creation and  deserialization  in using block for FileStream object
            // Write deserialization result to console
            var binFormater = new BinaryFormatter();
            // Define path for file
            // Create XmlSerializer serializer typeof Student
            // Implement  p serialization  in using block for FileStream object
            XmlSerializer xmlwriter = new XmlSerializer(typeof(Student));
            using (var streamBinForWrite = new FileStream("StudentFile.bin", FileMode.Create, FileAccess.Write, FileShare.None))
            {
                binFormater.Serialize(streamBinForWrite, studentVasya);
            }
            using (var streamXmlForWrite = new FileStream("StudentFile.xml", FileMode.Create, FileAccess.Write, FileShare.None))
            {
                xmlwriter.Serialize(streamXmlForWrite, studentVasya);
            }
            Console.WriteLine("Basic Deserialize:\n");
            using (var streamBinForRead = new FileStream("StudentFile.bin", FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                var vasyaFromBinFile = binFormater.Deserialize(streamBinForRead) as Student;
                if (vasyaFromBinFile != null) Console.WriteLine(vasyaFromBinFile.ToString());
            }
            Console.WriteLine("\nXML Deserialize:\n");
            // Create XmlSerializer deserializer typeof Student
            // Implement   deserialization  in using block for FileStream object
            // Write deserialization result to console
            using (var streamXmlForRead = new FileStream("StudentFile.xml", FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                var vasyaFromXmlFile = xmlwriter.Deserialize(streamXmlForRead) as Student;
                if (vasyaFromXmlFile != null) Console.WriteLine(vasyaFromXmlFile.ToString());
            }
            // NewtonJSON
            var filePath = Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), "StudentFile.json");
            var jsonConv = Newtonsoft.Json.JsonConvert.SerializeObject(studentVasya);
            using (var fileWriter = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None))
            {
               using (var streamWriter = new StreamWriter(fileWriter))
                {
                    streamWriter.WriteLine(jsonConv);
                }
            }
            Console.WriteLine("\nNewtonJson Deserialize:\n");
            var vasyaFromJsonFile = Newtonsoft.Json.JsonConvert.DeserializeObject<Student>(File.ReadAllText(filePath));
            if (vasyaFromJsonFile != null) Console.WriteLine(vasyaFromJsonFile.ToString());
            Console.ReadLine();
        }


    }
}
