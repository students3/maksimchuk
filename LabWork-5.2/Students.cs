﻿using System;
using System.Xml.Serialization;

namespace LabWork_5._2
{
    [Serializable] //Required by BinaryFormatter and SoapFormatter
    public class Student  //XMLSerializer needs the public class
    {
        //These fields will not be serialized by XmlSerialization
        [XmlIgnore]
        public string FirstName;
        public string LastName;
        [NonSerialized]
        public string Nationality;
        private string Address;
        private string Code;
        // Create SetAddress(string address, string code) method
        public void SetAddress(string address, string code)
        {
            this.Address = address;
            this.Code = code;
        }
        // Override ToString() method
        public override string ToString()
        {
            return  $"FirstName = {FirstName ?? "empty because [XmlIgnore]"}" +
                    $"\nLastName = {LastName}" +
                    $"\nNationality = {Nationality ?? "empty because [NonSerialized]"}" +
                    $"\nAddress = {Address ?? "empty because private"}" +
                    $"\nCode = {Code ?? "empty because private"}";
        }
    }
}
