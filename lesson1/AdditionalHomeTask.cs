﻿using System;

namespace lesson1
{
    class AdditionalHomeTask
    {
        public static void SomeTask1()
        {
            Console.WriteLine("Even numbers from 1 to 150: ");

            for (int i = 1; i <= 150; i++)
            {
                if (i % 2 == 0)
                {
                    Console.Write(i + " ");
                }
            }

            Console.WriteLine("\n" + new string('-', 55));
        }

        public static void SomeTask2()
        {
            Console.WriteLine("Odd numbers from 50 to 250: ");
            for (int i = 50; i <= 250; i++)
            {
                if (i % 2 != 0)
                {
                    Console.Write(i + " ");
                }
            }
            Console.WriteLine("\n" + new string('-', 55));
        }

        public static void SomeTask3()
        {
            Console.WriteLine("Position of 65 in arrTemp = {12, 21, 54, 65, 13, 587}");
            int[] arrTemp = { 12, 21, 54, 65, 13, 587 };
            int i = -1;
            int length = arrTemp.Length - 1;
            int search = 65;
            do
            {
                i++;
            } while (i <= length && arrTemp[i] != search);
            Console.WriteLine("arrTemp[" + i + "] == 65" + "\n" + new string('-', 55));
        }

        public static void SomeTask4()
        {
            int[] arrTemp = { 12, 21, 54, 65, 13, 587 };
            Console.WriteLine("arrTemp = {12, 21, 54, 65, 13, 587}\narrTemp * 2");
            Console.Write("");
            foreach (var item in arrTemp)
            {
                Console.Write(item * 2 + " ");
            }
            Console.WriteLine("\n" + new string('-', 55));
        }
    }
}
