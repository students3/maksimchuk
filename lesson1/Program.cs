﻿using Business;
using Business.Enums;
using Business.Interfaces;
using System.Linq;
using System;

namespace lesson1
{
    class Program
    {
        static void Main(string[] args)
        {
            IProduct[] products = GenerateProducts();
            SellProducts(products);
            PrintProducts(products);

            string headerSpace = "\n" + new string(' ', 20) + "\n";

            Console.WriteLine(headerSpace + "Sorting by Price" + headerSpace);

            IProduct[] sortedByPrice = SortProducts(products, SortingOrderEnum.Price);
            PrintProducts(sortedByPrice);

            Console.WriteLine(headerSpace + "Sorting by Name" + headerSpace);

            IProduct[] sortedByName = SortProducts(products, SortingOrderEnum.ProductName);
            PrintProducts(sortedByName);

            Console.WriteLine(headerSpace + "Sorting by Qty" + headerSpace);

            IProduct[] sortedByQty = SortProducts(products, SortingOrderEnum.Qty);
            PrintProducts(sortedByQty);

            Console.WriteLine(headerSpace);

            AdditionalHomeTask.SomeTask1();
            AdditionalHomeTask.SomeTask2();
            AdditionalHomeTask.SomeTask3();
            AdditionalHomeTask.SomeTask4();

            Console.ReadLine();
        }

        static IProduct[] GenerateProducts()
        {
            IProduct product1 = new Cake
            {
                ProductName = "Barnie",
                Manufacturer = "Barnie factory",
                Price = 7,
                DateCreated = new DateTime(2017, 2, 13),
                DateSold = DateTime.Now,
                Qty = (int)SellQtyEnum.InBatch5
            };
            IProduct product2 = new LapTop
            {
                ProductName = "X751L",
                Manufacturer = "ASUS",
                Price = 999,
                DateCreated = new DateTime(2017, 11, 11),
                DateSold = DateTime.Now,
                Qty = (int)SellQtyEnum.Single
            };
            IProduct product3 = new Cake
            {
                ProductName = "Teddy",
                Manufacturer = "AVK factory",
                Price = 9,
                DateCreated = new DateTime(2017, 10, 1),
                DateSold = DateTime.Now,
                Qty = (int)SellQtyEnum.InBatch3
            };
            IProduct product4 = new LapTop
            {
                ProductName = "XC550V",
                Manufacturer = "ASUS",
                Price = 600,
                DateCreated = new DateTime(2015, 10, 1),
                DateSold = DateTime.Now,
                Qty = (int)SellQtyEnum.Single
            };
            IProduct product5 = new MobilePhone
            {
                ProductName = "ZenFone 5",
                Manufacturer = "ASUS",
                DateCreated = new DateTime(2015, 10, 1),
                DateSold = DateTime.Now,
                Price = 150,
                Qty = (int)SellQtyEnum.Single,
                // set extra properties
                Fragile = "High"
            };
            IProduct product6 = new MobilePhone
            {
                ProductName = "A3",
                Manufacturer = "Samsung",
                DateCreated = new DateTime(2015, 10, 1),
                DateSold = DateTime.Now,
                Price = 400,
                Qty = (int)SellQtyEnum.Single,
                // extra properties Fraglie are not specified
            };
            return new IProduct[] { product1, product2, product3, product4, product5, product6 };
        }

        static void SellProducts(IProduct[] products)
        {
            IManager manager = null;

            for (int i = 0, length = products.Length; i < length; i++)
            {
                var item = products[i];
                if (item is Cake)
                {
                    manager = new CakeManager();
                }
                else if (item is LapTop)
                {
                    manager = new LapTopManager();
                }
                else if (item as MobilePhone != null)
                {
                    manager = new MobilePhoneManager();
                }
                // Qty is specified in GenerateProducts() using SellQtyEnum 
                if (manager != null) manager.SellProduct(item, item.Qty);
            }
        }

        static void PrintProducts(IProduct[] products)
        {
            // 2) Every class must have some extra properties, QtyForSugar (Cake), ZincQty (Laptop), Fragile (Telephone)
            //  use them when printing products
            for (int i = 0, length = products.Length; i < length; i++)
            {
                var item = products[i];
                string extraProperties = string.Empty;

                if (item is Cake)
                {
                    Cake cake = item as Cake;
                    extraProperties = "SugarQty: " + cake.GetSugarQty().ToString();
                }
                else if (item is LapTop)
                {
                    LapTop laptop = item as LapTop;
                    extraProperties = "ZincQty: " + laptop.GetZincQty().ToString();
                }
                else if (item is MobilePhone)
                {
                    MobilePhone mobilephone = item as MobilePhone;
                    //TODO: remove double statements (Done)
                    if (!String.IsNullOrEmpty(mobilephone.Fragile))
                    {
                        extraProperties = "Fraglie: " + mobilephone.Fragile;
                    }
                    else extraProperties = "Fraglie: Unknown";

                }
                string message = "Product : " + item.ProductName + "\n" +
                                 "Manufacturer : " + item.Manufacturer + "\n" +
                                 "sold on : " + item.DateSold?.ToString("yyyy MM dd") + "\n" +
                                 "in qty - " + item.Qty + "\n" +
                                 "Price : " + item.Price + "\n" +
                                 extraProperties;
                string line = "\n" + new string('-', 20);
                Console.WriteLine(message + line);
            }
        }

        static IProduct[] SortProducts(IProduct[] product, SortingOrderEnum orderBy) // enum for sorting
        {
            //1) switch by enum for sorting
            //TODO: IProduct[] sortedProduct = product (Done)
            IProduct[] sortedProduct = product;

            switch (orderBy)
            {
                case SortingOrderEnum.Price:
                    //TODO: use the following: sortedProduct = product.OrderBy(x => x.Price).ToArray(); (Done)
                    // <IProduct> is not necessary at the end of the statement (Done)
                    sortedProduct = product.OrderBy(x => x.Price).ToArray();
                    break;
                case SortingOrderEnum.ProductName:
                    sortedProduct = product.OrderBy(x => x.ProductName).ToArray();
                    break;
                case SortingOrderEnum.Qty:
                    sortedProduct = product.OrderBy(x => x.Qty).ToArray();
                    break;
            }
            return sortedProduct;
        }
    }
}