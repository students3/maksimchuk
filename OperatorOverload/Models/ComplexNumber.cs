﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperatorOverload.Models
{
    public struct ComplexNumber
    {
        public int RealPart { get; set; }
        public int ImaginaryPart { get; set; }


        public static ComplexNumber operator ++(ComplexNumber complexNumber)
        {
            ++complexNumber.RealPart;
            ++complexNumber.ImaginaryPart;
            return complexNumber;
        }

        public static ComplexNumber operator --(ComplexNumber complexNumber)
        {
            --complexNumber.RealPart;
            --complexNumber.ImaginaryPart;
            return complexNumber;
        }

        public static bool operator true(ComplexNumber complexNumber)
        {
            
            if (complexNumber.ImaginaryPart > 0)
            { 
                return true;
            }
            else
            {
                return false;
            }
                
        }

        public static bool operator false(ComplexNumber complexNumber)
        {

            if (complexNumber.ImaginaryPart < 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }


}
