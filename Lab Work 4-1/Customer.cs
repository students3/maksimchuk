﻿using System;

namespace Lab_Work_4_1
{
    class Customer
    {
        private string name;

        public Customer()
        {

        }

        public Customer(string name)
        {
            this.name = name;
        }

        public void NewGoodsArrivedToShop(object sender, NewGoodsArrivedEventArgs e)
        {
            if (sender != null && sender is OnlineShop)
            {
                Console.WriteLine($"{this.name} knows that the {e.GoodsName} product is available for sale");
            }
        }
    }
}
