﻿using System;
using System.Collections.Generic;

namespace Lab_Work_4_1
{
    class Program
    {
        static void Main(string[] args)
        {
            var shop = new OnlineShop();
            var customer1 = new Customer("John");
            var customer2 = new Customer("Greg");
            var customer3 = new Customer("Bob");

            shop.OnNewGoodsArrived += customer1.NewGoodsArrivedToShop;
            shop.OnNewGoodsArrived += customer2.NewGoodsArrivedToShop;
            shop.OnNewGoodsArrived += customer3.NewGoodsArrivedToShop;

            shop.AcceptNewGoods(new List<string>(new []{"Exhaust System", "Air Intake System" }));

            Console.ReadLine();
        }
    }
}
