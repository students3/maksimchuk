﻿using System;
using System.Collections.Generic;

namespace Lab_Work_4_1
{
    class OnlineShop
    {
        // always name event OnNewGoods
        public event EventHandler<NewGoodsArrivedEventArgs> OnNewGoodsArrived;

        public void AcceptNewGoods(List<string> listGoods)
        {
            foreach (var good in listGoods)
            {
                this.NewGoodsArrivedHandler(this, good);
            }
        }

        protected virtual void NewGoodsArrivedHandler(object sender, string goodsName)
        {
            if (OnNewGoodsArrived != null) this.OnNewGoodsArrived(sender, new NewGoodsArrivedEventArgs(goodsName));
        }
    }
}
