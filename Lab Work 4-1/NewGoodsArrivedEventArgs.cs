﻿using System;

namespace Lab_Work_4_1
{
    public class NewGoodsArrivedEventArgs : EventArgs
    {
        public NewGoodsArrivedEventArgs()
        {

        }
        public NewGoodsArrivedEventArgs(string GoodsName)
        {
            this.GoodsName = GoodsName;
        }
        public string GoodsName { get; set; }
    }
}
