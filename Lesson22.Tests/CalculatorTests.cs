﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lesson22.Tests
{
    [TestClass]
    public class CalculatorTests
    {
        [TestMethod]
        public void Multiply_2by7Expected18()
        {
            // Arrange
            var calculator = new Calculator();

            // Act
            var actual = calculator.Multiply(2, 7);

            // Assert
            Assert.AreEqual(14, actual);
        }

        [TestMethod]
        public void Sum_2and9Expected11()
        {
            // Arrange
            var calculator = new Calculator();

            // Act
            var actual = calculator.Sum(2, 9);

            // Assert
            Assert.AreEqual(11, actual);
        }

        [TestMethod]
        public void Sum_2and7Expected11()
        {
            // Arrange
            var calculator = new Calculator();

            // Act
            var actual = calculator.Sum(2, 7);

            // Assert
            Assert.AreEqual(9, actual);
        }

        [TestMethod]
        public void Divide_4by2_Excpected2()
        {
            // Arrange
            var calculator = new Calculator();

            // Act
            var act = calculator.Divide(4, 2);

            // Assert
            Assert.AreEqual(2, act);
        }

        [TestMethod]
        [ExpectedException(typeof(DivideByZeroException), "DivideByZero")]
        public void Divide_byZero()
        {
            // Arrange
            var calculator = new Calculator();

            // Act
            var act = calculator.Divide(4, 0);

            // Assert
            Assert.Fail();
        }
    }
}
