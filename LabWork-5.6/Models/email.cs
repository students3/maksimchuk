﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Hello_CodeFirst_Linq.Models
{
    [Table("email")]
    public class Email
    {
        [Key]
        public int Em_Id { get; set; }
        public string Em_value { get; set; }

        public string Lc_id { get; set; }

        // even email have 1 field for foreign key from lecturer,  virtual allow for it to be overridden in a derived class
        public virtual Lecturer Lecturer { get; set; }
    }
}
