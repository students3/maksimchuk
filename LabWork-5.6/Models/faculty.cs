﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Hello_CodeFirst_Linq.Models
{
    [Table("faculties")]
    public class Faculty
    {
        [Key]
        public string Facl_id { get; set; }
        public string Facl_name { get; set; }
        public string University { get; set; }
        public string State { get; set; }
        public string Country { get; set; }

        public virtual ICollection<Course> Course { get; set; }
    }
}
