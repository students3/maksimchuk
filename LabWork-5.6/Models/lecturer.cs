﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;


namespace Hello_CodeFirst_Linq.Models
{
   public class Lecturer
    {
        [Key]
        public string Lc_id { get; set; }
        public string Lc_fname { get; set; }
        public string Lc_lname { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }

        // field foreign key in email: 1 lecturer may have N emails
        public virtual ICollection<Email> Emails { get; set; }
        public virtual ICollection<Course_lecturer> Course_lecturers { get; set; }
    }
}
