﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Hello_CodeFirst_Linq.Models
{
    [Table("course_lecturers")]
    public class Course_lecturer
    {
        [Key]
        [Column(Order = 1)]
        public string Course_id { get; set; }
        [Key]
        [Column(Order = 2)]
        public string Lc_id { get; set; }
        public short Lc_order { get; set; }
        public decimal Share { get; set; }

        public virtual Lecturer Lecturer { get; set; }
        public virtual Course Course { get; set; }
    }
}
