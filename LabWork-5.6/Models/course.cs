﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Hello_CodeFirst_Linq.Models
{
    public class Course
    {
        [Key]
        public string Course_id { get; set; }
        public string Course_name { get; set; }
        public string Type { get; set; }
        public string Facl_id { get; set; }
        public Nullable<int> Size { get; set; }
        public Nullable<decimal> Marks { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<System.DateTime> Begin_date { get; set; }
        public short Contract { get; set; }

        public virtual Faculty Faculty { get; set; }
    }
}
