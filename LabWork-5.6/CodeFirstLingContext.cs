﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Hello_CodeFirst_Linq.Models;

namespace Hello_CodeFirst_Linq
{
    public class CodeFirstLingContext : DbContext
    {
        public CodeFirstLingContext()
            : base("LabWork_5-6")
        {

        }

        public DbSet<Lecturer> Lecturers { get; set; }
        public DbSet<Email> Emails { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<Faculty> Faculty { get; set; }
        public DbSet<Course_lecturer> Courses_leturers { get; set; }
    }
}
