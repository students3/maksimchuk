﻿using System;
using System.Linq;
using Hello_CodeFirst_Linq.Models;
using LabWork_5._6.Managers;

namespace Hello_CodeFirst_Linq
{
    public class Transaction_test
    {
        public void StartOwnTransactionWithinContext()
        {
            using (var ctx = new CodeFirstLingContext())
            {
                var dbManager = new DbManager(ctx);
                using (var dbContextTransaction = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        var lecturerList = ctx.Lecturers.ToList<Lecturer>();
                        //Perform create operation
                        Console.WriteLine("Perform create operation");
                        ctx.Lecturers.Add(new Lecturer() { Lc_id = "L_2", Lc_fname = "Second lecturer" });
                        //Execute Inser, Update & Delete queries in the database
                        ctx.SaveChanges();

                        var lects1 = ctx.Lecturers.ToList<Lecturer>();

                        //Perform Update operation
                        Console.WriteLine("Perform Update operation");
                        var lecturerList1 = ctx.Lecturers.ToList<Lecturer>();
                        //Lecturer lecturerToUpdate = lecturerList.Where(s => s.Lc_fname == "Second lecturer").FirstOrDefault<Lecturer>();
                        var listToUpdate = ctx.Lecturers.ToList().Where(lect => lect.State == "NK").ToList();
                        foreach (var item in listToUpdate)
                        {
                            item.Lc_lname += "_From NK";
                        }
                        //Execute Inser, Update & Delete queries in the database
                        ctx.SaveChanges();

                        dbContextTransaction.Commit();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        Console.WriteLine(e.InnerException.InnerException.Message);
                        dbContextTransaction.Rollback();
                    }
                }
            }
        }
    }
}


