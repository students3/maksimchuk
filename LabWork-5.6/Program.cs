﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data;
using System.Data.Entity;
using LabWork_5._6.Managers;
using Hello_CodeFirst_Linq.Models;

namespace Hello_CodeFirst_Linq
{
    class Program
    {

        static void Main(string[] args)
        {
            using (var ctx = new CodeFirstLingContext())
            {
               try
                {
                    DB_init(ctx);


                    var dbManager = new DbManager(ctx);
                    int a;

                    do
                    {
                        Console.WriteLine(@"Please,  type the number:
                        1.  Clear and insert to the DB by script
                        2.  Insert lecturer with email (one-to-many)
                        3.  Delete lecturer with email (one-to-many)
                        4.  Add new email to lecturer
                        5.  Update lecturer
                        6.  Transaction test");
                        try
                        {
                            Console.SetBufferSize(Console.LargestWindowWidth, Console.LargestWindowHeight);
                            a = int.Parse(Console.ReadLine());
                            switch (a)
                            {
                                case 1:
                                    string pathToDir = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                                    string filename = Path.Combine(pathToDir,"Scripts","insrt_ling.sql");
                                    Console.WriteLine("Clear and insert to the DB by script  ");
                                    dbManager.ExecuteSriptFile(filename);
                                    break;

                                case 2:
                                    Console.WriteLine("Insert lecturer with email (one-to-many) ");
                                    dbManager.CreateLectorWithEmails();
                                    break;

                                case 3:
                                    Console.WriteLine("Delete lecturer with email (one-to-many) ");
                                    dbManager.DeleteLector();
                                    break;

                                case 4:
                                    Console.WriteLine("Add new email to lecturer ");
                                    dbManager.AddNewEmailForLector();
                                    break;
                                case 5:
                                    Console.WriteLine("Update lecturer name ");
                                    dbManager.UpdateLecturerName();
                                    break;
                                case 6:
                                    Console.WriteLine("Transaction Test");
                                    var transaction = new Transaction_test();
                                    transaction.StartOwnTransactionWithinContext();
                                    break;
                                default:
                                    Console.WriteLine("Exit");
                                    break;
                            }
                        }
                        catch (System.Exception e)
                        {
                            Console.WriteLine("Error: " + e.Message);
                        }
                        finally
                        {
                            Console.ReadLine();
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("Press Spacebar to exit; press any key to continue");
                            Console.ForegroundColor = ConsoleColor.White;
                        }
                    }
                    while (Console.ReadKey().Key != ConsoleKey.Spacebar);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                Console.ReadKey();
            }
        }

        static void DB_init(CodeFirstLingContext ctx)
        {
            try
            {
                // Initialize database at this moment
                ctx.Database.Initialize(false);
            }
            catch (Exception)
            {
                Console.Write("Initialization error ?\r\n");
            }
        }

    }
}

