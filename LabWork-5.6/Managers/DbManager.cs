﻿using Hello_CodeFirst_Linq;
using Hello_CodeFirst_Linq.Models;
using System;
using System.IO;
using System.Linq;

namespace LabWork_5._6.Managers
{
    internal class DbManager
    {
        private readonly CodeFirstLingContext _ctx;
        public DbManager(CodeFirstLingContext ctx)
        {
            _ctx = ctx;
        }
        public void ExecuteSriptFile(string filePath)
        {
            if (System.IO.File.Exists(filePath))
            {
                var sqlText = File.ReadAllText(filePath);
                try
                {
                    _ctx.Database.ExecuteSqlCommand(sqlText);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }
        public void CreateLectorWithEmails()
        {
            string vlectorId = "L_"+Guid.NewGuid().ToString().Remove(4);
            var victorEmails = new Email[] { new Email { Em_value = "victor@gmail.com", Lc_id = vlectorId }, new Email { Em_value = "victor@yandex.ru", Lc_id = vlectorId } };
            var victor = new Lecturer
            {
                Lc_id = vlectorId,
                Lc_fname = "Victor",
                Lc_lname = "Zaharov",
                Address = "Spasskaya str. 1, apt. 6",
                Phone = "358-856-965",
                State = "NK",
                City = "Nikolaev",
                Emails = victorEmails
            };
            string blectorId = "L_" + Guid.NewGuid().ToString().Remove(4);
            var benEmails = new Email[] { new Email { Em_value = "ben@gmail.com", Lc_id = blectorId }, new Email { Em_value = "ben@yandex.ru", Lc_id = blectorId } };
            var ben = new Lecturer
            {
                Lc_id = blectorId,
                Lc_fname = "Ben",
                Lc_lname = "Walker",
                Address = "Chigrina str. 1, apt. 6",
                Phone = "999-856-965",
                State = "NK",
                City = "Nikolaev",
                Emails = benEmails
            };
            _ctx.Lecturers.Add(victor);
            _ctx.Lecturers.Add(ben);
            _ctx.SaveChanges();
            Console.WriteLine("Insert Lecturers with email finished");
        }
        public void DeleteLector()
        {
            var lectors = _ctx.Lecturers.ToList();
            if (lectors.Any())
            {
                var lector = lectors.Last();
                _ctx.Emails.RemoveRange(lector.Emails); // deleted emails of Lector in DBset
                _ctx.Lecturers.Remove(lector);
                _ctx.SaveChanges();
                Console.WriteLine("Lecturer was deleted");
            } else
            {
                Console.WriteLine("Lector was not found");
            }
            //public static void TestJoin()
            //{
            //    var query = from item1 in _ctx.Emails join item2 in _ctx.Lecturers;
            //}
        }

        internal void UpdateLecturerName()
        {
            var lectors = _ctx.Lecturers.ToList();
            if (lectors.Any())
            {
                var lastLector = lectors.Last();
                lastLector.Lc_fname = "NewName";
                _ctx.SaveChanges();
            }
            else
            {
                Console.WriteLine("Lectors was not found");
            }
        }

        public void AddNewEmailForLector()
        {
            var lectors = _ctx.Lecturers.ToList();
            if (lectors.Any())
            {
                var lector = lectors.Last();
                var idLastLector = lector.Lc_id;
                var newEmailForLastLector = new Email
                {
                    Em_value = "last@mail.ru",
                    Lc_id = idLastLector
                };
                lector.Emails.Add(newEmailForLastLector);
                _ctx.SaveChanges();
                Console.WriteLine("email for last Lecturer was added");
            }
            else
            {
                Console.WriteLine("Lector was not found");
            }
            //public static void TestJoin()
            //{
            //    var query = from item1 in _ctx.Emails join item2 in _ctx.Lecturers;
            //}
        }

    }
}
