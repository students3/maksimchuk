﻿CREATE TABLE [dbo].[UserAdditionalInfo] (
    [AdditionalInfoId] INT          IDENTITY (1, 1) NOT NULL,
    [Email]            VARCHAR (50) NULL,
    [Phone]            VARCHAR (50) NULL,
    [INN]              BIGINT       NULL,
    [UserId]           INT          NOT NULL,
    CONSTRAINT [PK_UserAdditionalInfo_AdditionalInfoId] PRIMARY KEY CLUSTERED ([AdditionalInfoId] ASC),
    CONSTRAINT [UK_UserAdditionalInfo_UserId] UNIQUE NONCLUSTERED ([UserId] ASC)
);

