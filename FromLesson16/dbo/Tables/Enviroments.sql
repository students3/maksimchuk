﻿CREATE TABLE [dbo].[Enviroments] (
    [EnviromentId]   INT          IDENTITY (1, 1) NOT NULL,
    [EnviromentName] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Enviroments_EnviromentId] PRIMARY KEY CLUSTERED ([EnviromentId] ASC)
);

