﻿CREATE TABLE [dbo].[Tasks] (
    [TaskId]       INT          IDENTITY (1, 1) NOT NULL,
    [TaskName]     VARCHAR (50) NOT NULL,
    [UserId]       INT          NOT NULL,
    [Completed]    BIT          DEFAULT ((0)) NOT NULL,
    [EnviromentId] INT          NULL,
    [FinishedDate] DATE         NULL,
    CONSTRAINT [PK_Projects_ProjectId] PRIMARY KEY CLUSTERED ([TaskId] ASC)
);





