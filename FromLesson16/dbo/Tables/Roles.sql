﻿CREATE TABLE [dbo].[Roles] (
    [RoleId]   INT          IDENTITY (1, 1) NOT NULL,
    [RoleName] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Roles_RoleId] PRIMARY KEY CLUSTERED ([RoleId] ASC)
);



