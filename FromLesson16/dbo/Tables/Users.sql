﻿CREATE TABLE [dbo].[Users] (
    [UserId]   INT       IDENTITY (1, 1) NOT NULL,
    [UserName] CHAR (10) NOT NULL,
    CONSTRAINT [PK_Users_PK] PRIMARY KEY CLUSTERED ([UserId] ASC)
);



