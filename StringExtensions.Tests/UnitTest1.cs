﻿using LabWork_II_1._3;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace StringExtensions.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void IsBlueBaseColorTest()
        {
            // Arrange
            string color = "blue";
            // Act
            bool actual = color.IsBaseColor();
            // Assert
            bool expected = false;

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void IsBlackBaseColorTest()
        {
            // Arrange
            string color = "black";
            // Act
            bool actual = color.IsBaseColor();
            // Assert
            bool expected = true;

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void IsWhiteBaseColorTest()
        {
            // Arrange
            string color = "white";
            // Act
            bool actual = color.IsBaseColor();
            // Assert
            bool expected = true;

            Assert.AreEqual(expected, actual);
        }

    }
}
