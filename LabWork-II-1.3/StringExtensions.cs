﻿using System;
using System.Linq;

namespace LabWork_II_1._3
{
    public static class StringExtensions
    {
        public static bool IsBaseColor(this string input)
        {
            if (string.IsNullOrEmpty(input)) return false;

            string[] baseColor = { "black", "white" };
            return baseColor.Any(x => string.Equals(x, input, StringComparison.InvariantCultureIgnoreCase));


            //bool flag = false;
            //string[] baseColor = { "black", "white" };
            //foreach (string item in baseColor)
            //{
            //    if (someString.Equals(item, StringComparison.InvariantCultureIgnoreCase)) flag = true;
            //}
            //return flag;
        }
    }
}
