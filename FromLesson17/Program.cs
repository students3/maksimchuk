﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromLesson17
{
    class Program
    {
        static void Main(string[] args)
        {
            testSqlConnection();
            Console.ReadLine();
        }
        public static void testSqlConnection()
        {
            string connstring = @"Data Source = (local); Encrypt = False; Integrated Security = True; User ID = DESKTOP - GIIUES6\mmt; Initial catalog=Lesson16;";
            string queryString = "Select * from dbo.Tasks";

            using (var dbconn = new SqlConnection(connstring))
            {
                dbconn.Open();
                SqlCommand command = new SqlCommand(queryString, dbconn);
                SqlDataReader reader = command.ExecuteReader();

                try
                {
                    while (reader.Read())
                    {

                        Console.WriteLine(string.Format("{0} {1} {2} {3}", reader[0], reader[1], reader[2], reader[3]));
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
        }
    }
}
