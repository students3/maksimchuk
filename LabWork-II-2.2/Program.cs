﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Threading;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace LabWork_II_2._2
{
    namespace Hello_tasks_stud
    {
        class Program
        {
            static void Main(string[] args)
            {
                int a;
                try
                {
                    do

                    {
                        Console.WriteLine("Please,  type the number:\n1.  Factorial\n2.  Factorial with Continuation\n3.  Factorial task async");
                        try
                        {
                            Console.SetBufferSize(Console.LargestWindowWidth, Console.LargestWindowHeight);
                            a = int.Parse(Console.ReadLine());
                            switch (a)
                            {
                                case 1:
                                    Console.WriteLine("Factorial");
                                    Fact_frk(4);
                                    break;
                                case 2:
                                    Console.WriteLine("Continuation ");
                                    Fact_cont(5);
                                    break;
                                case 3:
                                    Console.WriteLine("Factorial task async ");
                                    Task t = Fact_async(3);
                                    t.Wait();
                                    break;
                                default:
                                    Console.WriteLine("Exit");
                                    break;
                            }
                        }
                        catch (System.Exception e)
                        {
                            Console.WriteLine("Error: " + e.Message);
                        }
                        finally
                        {
                            Console.ReadLine();
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("Press Spacebar to exit; press any key to continue");
                            Console.ForegroundColor = ConsoleColor.White;
                       }
                    } while (Console.ReadKey().Key != ConsoleKey.Spacebar);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            static void CancelNotification()
            {
                Console.WriteLine("Cancellation request made");
            }

            //Create async Task Fact_async(int j) to calculate and output factorial
            async static Task Fact_async(int j)
            {
                // Create a cancellation token source
                var cts = new CancellationTokenSource();
                // Create a cancellation token
                var token = cts.Token;
                // Register a delegate for a callback when a  cancellation request is made
                var ctr = token.Register(() => CancelNotification());
                // Create a task with the cancellation token as argument as a lambda expression and the Factorial method
                var task1 = Task<int>.Factory.StartNew(() => Factorial(token, j), token);
                // If user presses Escape, request cancellation.
                var anyKey = Console.ReadKey();
                Console.WriteLine("Please Press Any Key...");
                if (anyKey.Key == ConsoleKey.Escape)
                {
                    // Cancel task
                    cts.Cancel();
                }
                // await task Fact_tsk
                await task1;
                // factorial value output
                Console.WriteLine("Factorial value = "+task1.Result);
            }

            // Create Fact_cont(int j) with  ContinueWith using
            //for factorial output to console
            static void Fact_cont(int j)
            {
                // Create a cancellation token source
                var cts = new CancellationTokenSource();
                // Create a cancellation token
                var token = cts.Token;
                // Create a task with the cancellation token as argument as a lambda expression and the Factorial method
                var task1 = Task<int>.Factory.StartNew(() => Factorial(token, j), token);
                // Register a delegate for a callback when a  cancellation request is made
                var ctr = token.Register(() => CancelNotification());
                // If user presses Escape, request cancellation.
                //PressAnyKey();
                Console.WriteLine("Please Press Any Key...");
                var anyKey = Console.ReadKey();
               if (anyKey.Key == ConsoleKey.Escape)
                {
                    // Cancel task
                    cts.Cancel();
                }
                // Create continuation 2 tasks using lambda expressions
                //with TaskContinuationOptions.OnlyOnRanToCompletion and  TaskContinuationOptions.NotOnRanToCompletion options
                var task2 = task1.ContinueWith(_ => Console.WriteLine("Continuation task 2"),
                    TaskContinuationOptions.OnlyOnRanToCompletion);
                var task3 = task2.ContinueWith(_ => Console.WriteLine("Continuation task 3"),
                    TaskContinuationOptions.NotOnRanToCompletion);
                // Task.WaitAll for these two tasks
                Task.WaitAll();
                //IsCanceled properies output
                Console.WriteLine($"task1 IsCanceled : " + task1.IsCanceled);
                Console.WriteLine($"task2 IsCanceled : " + task2.IsCanceled);
                Console.WriteLine($"task3 IsCanceled : " + task3.IsCanceled);
            }

            // Create void Fact_frk(int j) to calculate factorial
            static void Fact_frk(int j)
            {
                // Create a cancellation token source
                var cts = new CancellationTokenSource();
                // Create a cancellation token
                var token = cts.Token;
                // Create a task with the cancellation token as argument as a lambda expression and the Factorial method
                var task1 = Task<int>.Factory.StartNew(() => Factorial(token, j), token);
                // Register a delegate for a callback when a  cancellation request is made
                var ctr = token.Register(() => CancelNotification());
                // If user presses Escape, request cancellation.
                Console.WriteLine("Please Press Any Key...");
                var anyKey = Console.ReadKey();
                if (anyKey.Key == ConsoleKey.Escape)
               {
                    // Cancel task
                    cts.Cancel();
                }
                else
               {
                    //factorial value output
                    Console.WriteLine("Factorial result : " + task1.Result);
                }
            }

            // Create Facfactoristorial(CancellationToken ct, int num) to calculate recursively
            static int Factorial(CancellationToken ct, int num)
            {
                // create if block to check cancellation requested
                // throw if cancellation requested
                try
                {
                    if (ct.IsCancellationRequested) ct.ThrowIfCancellationRequested();
                }
                catch (OperationCanceledException ex)
                {
                    Console.WriteLine("OperationCanceledException Because of pressing of key Escape");
                }
                finally
                {
                    //argument output
                    //simulate long work
                    Console.WriteLine("Sleep...");
                    Thread.Sleep(5000);
                    //return recursive calculation of factorial
                    //use multiplication to  PressAnyKey() method result to catch key press
                    Console.WriteLine("Please Press Any Key...");
                    PressAnyKey();
                }
                return (num == 0) ? 1 : num * Factorial(ct, num - 1);
            }

            static int PressAnyKey()
            {
                KeyboardSend.KeyDown(Keys.Back);
                return 1;
            }

            static class KeyboardSend
            {
                //Simulation of a keyboard input
                [DllImport("user32.dll")]
                private static extern void keybd_event(byte bVk, byte bScan, int dwFlags, int dwExtraInfo);

                private const int KEYEVENTF_EXTENDEDKEY = 1;
                private const int KEYEVENTF_KEYUP = 2;

                public static void KeyDown(Keys vKey)
                {
                    keybd_event((byte) vKey, 0, KEYEVENTF_EXTENDEDKEY, 0);
                }

                public static void KeyUp(Keys vKey)
                {
                    keybd_event((byte) vKey, 0, KEYEVENTF_EXTENDEDKEY | KEYEVENTF_KEYUP, 0);
                }
            }
        }
    }
}