﻿using System;

namespace Lab_Wotk_4_2_Step2.Extensions
{
    public static class CurryList
    {
        public static Func<TArg2, Func<TArg1, TResult>> Bnd<TArg1, TArg2, TResult>(this Func<TArg1, TArg2, TResult> f)
        {
            return (y) => ((x) => f(x, y));
        }
    }
}
