﻿using System;
using System.Collections.Generic;
using Lab_Wotk_4_2_Step2.Extensions;
namespace Lab_Wotk_4_2_Step2
{
    class Program
    {
        static void Main(string[] args)
        {
            var source = new List<double> { 1.0, 2.4, 34.9, 9.02, 7.0 };
            var result = new List<double>();

            Func<double, double, double> f = (x, y) => x - y;
            var fBnd = f.Bnd().Invoke(2.0);

            Console.WriteLine("Source list");

            foreach (var item in source)
            {
                Console.Write("{0}; ", item);
                result.Add(fBnd.Invoke(item));
            }

            Console.WriteLine("\nResult List");

            foreach (var item in result)
            {
                Console.Write("{0}; ", item);
            }

            Console.ReadLine();
        }
    }
}
