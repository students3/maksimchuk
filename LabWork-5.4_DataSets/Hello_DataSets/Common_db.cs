﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace Hello_DataSets
{
    public class Common_db
    {
        // define string MyConnectionString
        //string connectionString=@"Data Source=(LocalDB)\v11.0;AttachDbFilename=|DataDirectory|\datasets\tect_datasets_db.mdf;Integrated Security=True;Connect Timeout=30";
        string connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=G:\torrent-files\(begin)ado_net_154\ado_net_174\Begin\tect_datasets_db.mdf;Integrated Security=True;Connect Timeout=30";
        public string MyConnectionString = null;
        // implement Common_db constructor with string parameter for connection string
        public Common_db(string connectionString)
        {
            MyConnectionString = connectionString;
        }
        // implement bool MyTable_delete(DataTable usr_table, string key, string key_value) method
        // with parameters fot DataTable , string key name, string key value to delete row
        // define bool result and initiate it with false
        public bool MyTable_delete(DataTable usr_table, string key, string key_value)
        {
            // define bool result and initiate it with false
            bool deleteResult = false;
            // in try-catch block implement using block for work new SglConnection with connection string MyConnectionString
            try
            {
                using (var sqlConn = new SqlConnection(connectionString))
                {
                    // create SqlCommand object for SglConnection object
                    var sqlCommand = sqlConn.CreateCommand();
                    // define its CommandText like sql query to select all from "usr_table.tablename" from database
                    sqlCommand.CommandText = $"SELECT * FROM {usr_table.TableName}";
                    // create SqlDataAdapter object associated with SqlCommand object
                    var sqlAdapter = new SqlDataAdapter(sqlCommand);
                    // populate usr_table by data from database
                    sqlAdapter.Fill(usr_table);
                    // define usr_table primary key by new DataColumn[], initiate it by key value
                    usr_table.PrimaryKey = new DataColumn[] { usr_table.Columns[key] };
                    // accept changes for usr_table
                    usr_table.AcceptChanges();
                    //define DataRow object as the usr_table row with key value which equals key_value
                    var dataRow = usr_table.Rows.Find(key_value);
                    // create string for sql query to delete this row in the database table
                    var deleteQuery = $"DELETE FROM {usr_table.TableName} WHERE {key} = '{key_value}'";
                    // in try-catch block open connection, create adapter UpdateCommand  using connection CreateCommand method
                    // Open connection
                    try
                    {
                        sqlConn.Open();
                        // define UpdateCommand for adapter
                        // define its CommandText for string to delete row
                        sqlAdapter.DeleteCommand = sqlConn.CreateCommand();
                        sqlAdapter.DeleteCommand.CommandText = deleteQuery;
                        sqlAdapter.DeleteCommand.ExecuteNonQuery();
                        deleteResult = true;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        return false;
                    }
                }
                return deleteResult;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        public bool MyTable_read(DataTable usr_table)
        {
            bool fillResult = true;
            using (SqlConnection MySqlConnection = new SqlConnection(connectionString))
            {
                // create SqlCommand object for SglConnection object
                SqlCommand MySqlCommand = MySqlConnection.CreateCommand();
                // define its CommandText like sql query to select all from "usr_table.tablename" from database
                MySqlCommand.CommandText = "SELECT * FROM " + usr_table.TableName;
                SqlDataAdapter MySqlAdapter = new SqlDataAdapter(MySqlCommand);
                MySqlAdapter.Fill(usr_table);
            }
            if (usr_table.Rows.Count == 0)
            {
                fillResult = false;
            }
            return fillResult;
        }
        // implement bool MyTable_update(DataTable usr_table, string key, string key_value, string clmn, string clmn_value) method
        // with parameters fot DataTable , string key name, string key value, string clmn, string clmn_value to update row

        public bool MyTable_update(DataTable usr_table, string key, string key_value, string clmn, string clmn_value)
        {
            // define bool result and initiate it with false
            bool updateResult = true;
            using (SqlConnection MySqlConnection = new SqlConnection(connectionString))
            {
                string query = $"SELECT * FROM {usr_table.TableName}";
                SqlDataAdapter MySqlDataAdapter = new SqlDataAdapter(query, MySqlConnection);
                // populate usr_table by data from database
                MySqlDataAdapter.Fill(usr_table);
                // define usr_table primary key by new DataColumn[], initiate it by key value
                usr_table.PrimaryKey = new DataColumn[] { usr_table.Columns[key] };
                // accept changes for usr_table
                usr_table.AcceptChanges();
                //define DataRow object as the usr_table row with key value which equals key_value
                DataRow cur_course = usr_table.Rows.Find(key_value);
                Console.WriteLine("Key: {0}, Column {1}: {2} ", cur_course[key],
                clmn, cur_course[clmn]);
                // create string for sql query to update this row in the database table
                string sqlstr = "UPDATE " + usr_table.TableName + " SET " + clmn +
                "='" + clmn_value + "' WHERE " + key + "='" + key_value + "'";
                // in try-catch block open connection, create adapter UpdateCommand  using connection CreateCommand method
                try
                {
                    // Open connection
                    MySqlConnection.Open();
                    // define UpdateCommand for adapter
                    MySqlDataAdapter.UpdateCommand = MySqlConnection.CreateCommand();
                    // define its CommandText for string to delete row
                    MySqlDataAdapter.UpdateCommand.CommandText = sqlstr;
                    // execute query
                    MySqlDataAdapter.UpdateCommand.ExecuteNonQuery();
                    // assign true for result
                    updateResult = true;
                    Console.WriteLine("Row updated to " + clmn_value + "! ");
                }
                catch (Exception ex)
                {
                    // exception message output
                    Console.WriteLine(ex.Message);
                    return updateResult;
                }
                // return
                return updateResult;
            }
        }

        // implement bool MyTable_insert_bldr(string usr_table, string key,  string[] clmn, string[] clmn_value) method
        // with parameters fot DataTable , string key name, string clmn, string clmn_value to insert row

        public bool MyTable_insert_bldr(string usr_table, string key, string[] clmn, string[] clmn_value)
        {
            // define bool result and initiate it with false
            bool insertResult = false;
            // in try-catch block implement using block for work new SglConnection with connection string MyConnectionString
            try
            {
                using (var sqlConn = new SqlConnection(connectionString))
                {
                    // create SqlCommand object for SglConnection object
                    var sqlCommand = sqlConn.CreateCommand();
                    // define its CommandText like sql query to select all from "usr_table" from database
                    sqlCommand.CommandText = $"SELECT * FROM {usr_table}";
                    // create SqlDataAdapter object associated with SqlCommand object
                    var sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                    // create new  SqlCommandBuilder  object associated with adapter
                    var sqlComBldr = new SqlCommandBuilder(sqlDataAdapter);
                    // create new DataSet
                    var ds = new DataSet();
                    // fill it using usr_table
                    sqlDataAdapter.Fill(ds, usr_table);
                    // define DataTable object  from DataSet with usr_table name
                    var dt = ds.Tables[usr_table];
                    // define its primary key by new DataColumn[], initiate it by key value
                    dt.PrimaryKey = new DataColumn[] { dt.Columns[key] };
                    // accept changes for DataTable object
                    dt.AcceptChanges();
                    // create next key value using Next_key_gen method
                    string next_key = string.Empty;
                    next_key = Next_key_gen(dt, key);
                    // declare DataRow object
                    // asiign DataTable object NewRow method result to it
                    var workRow = dt.NewRow();
                    // define it key value
                    workRow[key] = next_key;
                    // assign DataRow object column values in the for loop
                    for (int i = 0, length = clmn.Length; i < length; i++)
                    {
                        // ????
                        workRow[clmn[i]] = clmn_value[i];
                    }
                    // add this row to the DataTable object
                    dt.Rows.Add(workRow);
                    // call adapter Update method to update usr_table
                    sqlDataAdapter.Update(ds, usr_table);
                    // assign true for result
                    insertResult = true;
                    // return result
                    return insertResult;
                }
            }
            catch (Exception ex)
            {
                // exception message output
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        // implement bool MyTable_update_bldr(string usr_table, string key, string key_value, string clmn, string clmn_value) method
        // with parameters fot table name , string key name,  string key_value,string clmn, string clmn_value to update the table
        public bool MyTable_update_bldr(string usr_table, string key, string key_value, string clmn, string clmn_value)
        {
            // define bool result and initiate it with false
            bool updateResult = false;
            // in try-catch block implement using block for work new SglConnection with connection string MyConnectionString
            try
            {
                using (var sqlCon = new SqlConnection(connectionString))
                {
                    // create SqlCommand object for SglConnection object
                    var sqlCommand = sqlCon.CreateCommand();
                    // define its CommandText like sql query to select all from "usr_table" from database
                    sqlCommand.CommandText = $"SELECT * FROM {usr_table}";
                    // create SqlDataAdapter object associated with SqlCommand object
                    SqlDataAdapter MySqlDataAdapter = new SqlDataAdapter(sqlCommand);
                    // Set up the CommandBuilder
                    var comBuilder = new SqlCommandBuilder(MySqlDataAdapter);
                    // create new DataSet
                    DataSet ds = new DataSet();
                    // fill it using usr_table
                    MySqlDataAdapter.Fill(ds, usr_table);
                    var dt = ds.Tables[usr_table];
                    // define usr_table primary key by new DataColumn[], initiate it by key value
                    dt.PrimaryKey = new DataColumn[] { dt.Columns[key] };
                    // accept changes for usr_table
                    dt.AcceptChanges();

                    // create DataRow object and assign  DataTable object Rows.Find(key_value) result to it
                    DataRow cur_course = dt.Rows.Find(key_value);
                    Console.WriteLine("Key: {0}, Column {1}: {2} ", cur_course[key], clmn, cur_course[clmn]);
                    // assign to its clmn column clmn_value
                    cur_course[clmn] = clmn_value;
                    // call adapter Update method to update usr_table
                    MySqlDataAdapter.Update(ds, usr_table);
                    // assign true for result
                    updateResult = true;
                    // return result
                    return updateResult;
                }
            }
            catch (Exception e)
            {
                // exception message output
                Console.WriteLine(e.Message);
                //return false
                return false;
            }
        }

        // implement string Next_key_gen(DataTable dt, string key ) method  to receive the next key value
        public string Next_key_gen(DataTable dt, string key)
        {
            // create List<string> new object
            var prKeyList = new List<string>();
            // in foreach loop for DataRow item in dt.Rows add key values to List
            foreach (DataRow row in dt.Rows)
            {
                prKeyList.Add(row[key].ToString());
            }
            // sort the List object
            prKeyList.Sort();
            // find the last key value
            string last_key_value = prKeyList[prKeyList.Count - 1];
            Console.WriteLine("Max key: {0} . Write next key value: \r\n", last_key_value);
            return Console.ReadLine();
        }

        // implement bool MyTable_update_ds(string usr_table, string key, string key_value, string clmn, string clmn_value) method
        // with parameters fot table name , string key name,  string key_value, string clmn, string clmn_value to update the table
        public bool MyTable_update_ds(string usr_table, string key, string key_value, string clmn, string clmn_value)
        {
            // define bool result and initiate it with false
            bool updateResult = false;
            using (var sqlCon = new SqlConnection(connectionString))
            {
                // create SqlCommand object for SglConnection object
                var sqlCommand = sqlCon.CreateCommand();
                // define its CommandText like sql query to select all from "usr_table" from database
                sqlCommand.CommandText = $"SELECT * FROM {usr_table}";
                // create SqlDataAdapter object associated with SqlCommand object
                SqlDataAdapter MySqlDataAdapter = new SqlDataAdapter(sqlCommand);
                // create new DataSet
                var ds = new DataSet();
                // fill it using usr_table
                MySqlDataAdapter.Fill(ds, usr_table);
                // define DataTable object  from DataSet with usr_table name
                DataTable dt = ds.Tables[usr_table];
                // define its primary key by new DataColumn[], initiate it by key value
                dt.PrimaryKey = new DataColumn[] { dt.Columns[key] };
                // accept changes for DataTable object
                dt.AcceptChanges();
                // create DataRow object and assign  DataTable object Rows.Find(key_value) result to it
                DataRow cur_course = dt.Rows.Find(key_value);
                Console.WriteLine("Key: {0}, Column {1}: {2} ", cur_course[key],
                clmn, cur_course[clmn]);
                // create string for sql query to update this row in the database table
                string sqlstr = "UPDATE " + usr_table + " SET " + clmn +
                "='" + clmn_value + "' WHERE " + key + "='" + key_value + "'";
                // in try-catch block open connection, create adapter UpdateCommand  using connection CreateCommand method
                try
                {
                    // Open connection
                    sqlCon.Open();
                    // define UpdateCommand for adapter
                    MySqlDataAdapter.UpdateCommand = sqlCon.CreateCommand();
                    // define its CommandText for string to delete row
                    MySqlDataAdapter.UpdateCommand.CommandText = sqlstr;
                    // execute query
                    MySqlDataAdapter.UpdateCommand.ExecuteNonQuery();
                    // assign true for result
                    updateResult = true;
                    Console.WriteLine("Row updated to " + clmn_value + "! ");
                }
                catch (Exception ex)
                {
                    // exception message output
                    Console.WriteLine(ex.Message);
                    return updateResult;
                }
                // return
                return updateResult;
            }

        }

        // implement MyConnect() method to open and check the connection

        // define bool result and initiate it with false
        // in try-catch block implement using block for work new SglConnection with connection string MyConnectionString

        // Open connection
        // assign true for result

        // return result

        // exception message output
        // return false
        public bool MyConnect()
        {
            bool connectResult = false;
            // in try-catch block implement using block for work new SglConnection with connection string MyConnectionString
            try
            {
                using (SqlConnection MyConn = new SqlConnection(MyConnectionString))
                {
                    MyConn.Open();
                    connectResult = true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
            return connectResult;
        }
    }
}


















