﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace Hello_DataSets
{
    public class DB_work
    {
        // declare    Common_db MyDBtest
        Common_db MyDBTest;
        // implement DB_work(string conn) constructor to initiate MyDBtest
        public DB_work(string conn)
        {
            this.MyDBTest = new Common_db(conn);
        }

        // implement  void DB_conn() to check the MyDBtest
        public void DB_conn()
        {
            if (MyDBTest.MyConnect())
            {
                Console.WriteLine("DB connected");
            }
            else Console.WriteLine("DB not connected");
        }
        // try check connection using MyConnect() and MyDisConnect()



        // implement void Courses_Update_ds(string table_name, string key, string key_value, string clmn, string clmn_value)
        // to update table_name using MyDBtest.MyTable_update_ds method with parameters in try-catch block
        public void Courses_Update_ds(string table_name, string key, string key_value, string clmn, string clmn_value)
        {
            MyDBTest.MyTable_update_ds(table_name, key, key_value, clmn, clmn_value);
        }


        // implement void Courses_Insert_bldr(string table_name, string key,  string [] clmn, string[] clmn_value)
        // to insert into table_name using MyDBtest.MyTable_insert_bldr method with parameters in try-catch block
        public void Courses_Insert_bldr(string table_name, string key, string[] clmn, string[] clmn_value)
        {
            MyDBTest.MyTable_insert_bldr(table_name, key, clmn, clmn_value);
        }


        // implement void Courses_Update_bldr(string table_name, string key, string key_value, string clmn, string clmn_value)
        // to update table_name using MyDBtest.MyTable_update_bldr method with parameters in try-catch block
        public void Courses_Update_bldr(string table_name, string key, string key_value, string clmn, string clmn_value)
        {
            MyDBTest.MyTable_update_bldr(table_name, key, key_value, clmn, clmn_value);
        }


        // implement void Courses_Update(string table_name, string key, string key_value, string clmn, string clmn_value)
        // to update table_name using MyDBtest.MyTable_update method with parameters in try-catch block
        public void Courses_Update(string table_name, string key, string key_value, string clmn, string clmn_value)
        {
            //create new DataTable object and define its TableName property
            var dt = new DataTable();
            dt.TableName = table_name;
            //call MyDBtest.MyTable_update with parameters
            MyDBTest.MyTable_update(dt, key, key_value, clmn, clmn_value);
        }

        // implement void Courses_Read(string table_name) method
        // to read table_name using MyDBtest.MyTable_read method with parameters in try-catch block
        public void Courses_Read(string table_name)
        {

            //create new DataTable object and define its TableName property
            var dt = new DataTable();
            dt.TableName = table_name;
            //call MyDBtest.MyTable_read with parameter
            MyDBTest.MyTable_read(dt);
            //foreach DataRow item  write its value to the console
            using (var sqlConn = new SqlConnection(MyDBTest.MyConnectionString))
            {
                string query = $"SELECT * FROM {table_name}";
                var sqlCommand = new SqlCommand(query, sqlConn);
                sqlConn.Open();
                SqlDataReader reader = sqlCommand.ExecuteReader();
                object[] rowContent = new object[reader.FieldCount];
                // Call Read before accessing data.

                while (reader.Read())
                {
                    reader.GetValues(rowContent);
                    Console.WriteLine(string.Join(" | ", rowContent));
                }
            }
        }
        // implement Courses_Delete(string table_name, string key, string key_value) method
        // to delete row from the table_name using MyDBtest.MyTable_delete method with parameters in try-catch block
        public void Courses_Delete(string table_name, string key, string key_value)
        {
            var dt = new DataTable();
            //create new DataTable object and define its TableName property
            dt.TableName = table_name;
            //call MyDBtest.MyTable_delete with parameters
            MyDBTest.MyTable_delete(dt, key, key_value);
        }
    }
}
