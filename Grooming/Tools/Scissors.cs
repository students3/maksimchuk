﻿using Grooming.Interfaces;

namespace Grooming.Tools
{
    public class Scissors : ITool
    {
        public string ToolName => "Scissors";
    }
}
