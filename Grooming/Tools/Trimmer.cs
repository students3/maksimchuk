﻿using Grooming.Interfaces;

namespace Grooming.Tools
{
    public class Trimmer : ITool
    {
        public string ToolName => "Trimmer";
    }
}
