﻿using Grooming.Interfaces;

namespace Grooming.Drinks
{
    class Tea : IBeverage
    {
        public string BeverageName => "Tea";
    }
}
