﻿using Grooming.Interfaces;

namespace Grooming.Drinks
{
    class Coffe : IBeverage
    {
        public string BeverageName => "Coffe";
    }
}
