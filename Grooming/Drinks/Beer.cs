﻿using Grooming.Interfaces;

namespace Grooming.Drinks
{
    class Beer : IBeverage
    {
        public string BeverageName => "Beer";
    }
}
