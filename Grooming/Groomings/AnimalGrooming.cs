﻿using Grooming.Drinks;
using Grooming.Interfaces;
using System;
using System.IO;

namespace Grooming.Groomings
{
    public abstract class AnimalGrooming<T> : IGrooming<T> where T : IAnimal
    {


        public void MakeGrooming<V, U>(T animal, V tool, U tool2)
            where V : ITool
            where U : ITool
        {
            if (animal.Weight <= 1)
            {
                System.Console.WriteLine("Sorry, we can't groom your: " + animal.Name +
                                         " Its' too small");
            } else
            {
            this.OfferBeverageToOwner(animal);
            System.Console.WriteLine("Making grooming for: " + animal.Name +
                                    " with tool: " + tool.ToolName + " and " + tool2.ToolName);
            this.SayAboutDiscount();

            }

        }

        public void MakeGrooming(T animal)
        {
            if (animal.Weight <= 1)
            {
                Console.WriteLine($"Sorry, we can't groom your: {animal.Name}. It's too small");
            }
            this.OfferBeverageToOwner(animal);
            SayAboutDiscount();
        }

        private void OfferBeverageToOwner(T animal)
        {
            IBeverage drink = null;
            string animalType = Path.GetExtension(animal.GetType().ToString()).Replace(".", String.Empty);
            switch (animal.GetType().Name)
            {
                case "Cat":
                    drink = new Tea();
                    break;
                case "Dog":
                    drink = new Coffe();
                    break;
                case "Panda":
                    drink = new Beer();
                    break;
                case "Deer":
                    drink = new Coffe();
                    break;
            }
            Console.WriteLine($"Offering {drink.BeverageName} to owner");
        }
        private void SayAboutDiscount()
        {
            Console.WriteLine("Come to us next time and you'll get discount");
        }
    }
}
