﻿
namespace Grooming.Interfaces
{
    public interface ITool
    {
        string ToolName { get; }
    }
}
