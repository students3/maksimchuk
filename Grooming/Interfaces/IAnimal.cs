﻿
namespace Grooming.Interfaces
{
    public interface IAnimal
    {
        string Name { get; set; }
        int Weight { get; set; }
    }
}
