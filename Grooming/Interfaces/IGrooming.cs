﻿
namespace Grooming.Interfaces
{
    public interface IGrooming<T>
    {
        void MakeGrooming(T animal);
        void MakeGrooming<V, U>(T animal, V tool, U tool2) 
            where V : ITool 
            where U : ITool;
    }
}
