﻿
namespace Grooming.Interfaces
{
    public interface IBeverage
    {
        string BeverageName { get; }
    }
}
