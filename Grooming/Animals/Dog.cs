﻿using Grooming.Interfaces;

namespace Grooming.Animals
{
    public class Dog : IAnimal
    {
        public string Name { get; set; }
        public int Weight { get; set; }
        public Dog()
        {
            Weight = 12;
        }
        public Dog(int weight)
        {
            this.Weight = weight;
        }
    }
}
