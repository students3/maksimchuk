﻿using Grooming.Interfaces;

namespace Grooming.Animals
{
    public class Deer : IAnimal
    {
        public string Name { get; set; }
        public int Weight { get; set; }
        public Deer()
        {
            Weight = 6;
        }
    }
}
