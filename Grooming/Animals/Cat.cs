﻿using Grooming.Interfaces;

namespace Grooming.Animals
{
    public class Cat : IAnimal
    {
        public string Name { get ; set ; }
        public int Weight { get ; set ; }
        public Cat()
        {
            Weight = 6;
        }
        public Cat(int weight)
        {
            this.Weight = weight;
        }
    }
}
