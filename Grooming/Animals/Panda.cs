﻿using Grooming.Interfaces;


namespace Grooming.Animals
{
    public class Panda : IAnimal
    {
        public string Name { get ; set ; }
        public int Weight { get ; set ; }
        public Panda()
        {
            Weight = 22;
        }
        public Panda(int weight)
        {
            this.Weight = weight;
        }
    }
}
