﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grooming.Interfaces;
using Grooming.Groomings;
using Grooming.Animals;
using Grooming.Tools;
using System.IO;

namespace Grooming
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Groomings

            var CatGrooming = new CatGrooming();
            var DogGrooming = new DogGrooming();
            var PandaGrooming = new PandaGrooming();
            var DeerGrooming = new DeerGrooming();

            #endregion

            #region Animals

            var Cat = new Cat { Name = "Garfield", Weight = 3 };
            var Kitty = new Cat { Name = "Johnsy", Weight = 1 };
            var Dog = new Dog {Name = "Rex", Weight = 15 };
            var Puppy = new Dog { Name = "Marcy", Weight = 1 };
            var Panda = new Panda { Name = "Farlie", Weight = 25 };
            var Deer = new Deer { Name = "Lanny", Weight = 75 };

            #endregion 

            #region Tools

            var Comb = new Comb();
            var Rasp = new Rasp();
            var Scissors = new Scissors();
            var Trimmer = new Trimmer();

            #endregion

            #region clients queue

            Queue<IAnimal> clientsQueue = new Queue<IAnimal>();
            clientsQueue.Enqueue(Cat);
            clientsQueue.Enqueue(Kitty);
            clientsQueue.Enqueue(Dog);
            clientsQueue.Enqueue(Puppy);
            clientsQueue.Enqueue(Panda);
            clientsQueue.Enqueue(Deer);

            #endregion

            while (clientsQueue.Count > 0)
            {
                IAnimal animal = clientsQueue.Dequeue();
                string animalType = Path.GetExtension(animal.GetType().ToString()).Replace(".", String.Empty);
                switch (animalType)
                {
                    case "Cat":
                        CatGrooming.MakeGrooming(animal as Cat, Scissors, Comb);
                        break;
                    case "Dog":
                        DogGrooming.MakeGrooming(animal as Dog, Scissors, Comb);
                        break;
                    case "Deer":
                        DeerGrooming.MakeGrooming(animal as Deer, Rasp, Trimmer);
                        break;
                    case "Panda":
                        PandaGrooming.MakeGrooming(animal as Panda, Scissors, Trimmer);
                        break;
                }
                Console.WriteLine();
            }
            Console.ReadLine();
        }

    }
}
