﻿using LabWork_2._4;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MorseTestFromLW_2._4
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void MorseTest()
        {
            // Arrange
            var morseCoder = new Morse_matrix();

            // Act
            string[] sos = { "...  ", "---  ", "...  " };
            var actual = morseCoder.DecryptString(sos);

            // Assert
            var expected = "sos";
            Assert.AreEqual(expected, actual);
        }
    }
}
