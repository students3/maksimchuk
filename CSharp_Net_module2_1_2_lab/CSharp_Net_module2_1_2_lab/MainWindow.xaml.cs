﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CSharp_Net_module2_1_2_lab
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // 15) Declare objects DataSet, DataAdapter and others
        private DataTable _dtSource;
        public MainWindow()
        {

            InitializeComponent();

            // 9) Set Background for button; use Brushes
            btn_Multi.Background = Brushes.Cornsilk;

            // 16) Set DataAdapter and DataSet
            // Note: Don't forget to use connection string

            // 17) Add datacontext of datagrid with code:
            // dataGridView.DataContext = da.Tables[“MyTable"];
            // Where
            // dataGridView – name (object) of used datagrind
            // da – object of DataSet
            // “MyTable” – table name

        }

        // 11) Add event handler on button click (for colored button)
        // 12) Add new window to project
        // In new window add XAML to show backgroud image
        // (Done)
        // 13) invoke method ShowDialig() for new window

        // 18) Add event handler on button click (for 2nd button)
        private void Button_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                Window secondWindow = new SeconWindow();
                secondWindow.ShowDialog();
            }
            if (e.RightButton == MouseButtonState.Pressed)
            {
                MessageBox.Show("RightButton Clicked", "Click Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        private DataTable GenerateDatable()
        {
            var dt = new DataTable();
            var dc = dt.Columns.Add("TaskId", typeof(int));
            dc.AutoIncrement = true;
            dc.AutoIncrementSeed = 1;

            dt.Columns.Add("TaskName", typeof(string));
            dt.Columns.Add("UserName", typeof(string));

            var dr = dt.NewRow();
            //dr["TaskId"] = 1;
            dr["TaskName"] = "Water flowers";
            dr["UserName"] = "Sergey";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            //dr["TaskId"] = 2;
            dr["TaskName"] = "Go to the gym";
            dr["UserName"] = "Kostya";
            dt.Rows.Add(dr);


            dr = dt.NewRow();
            //dr["TaskId"] = 2;
            dr["TaskName"] = "Go to the gym";
            dr["UserName"] = "Igor";
            dt.Rows.Add(dr);


            dt.AcceptChanges();

            return dt;
        }
        // 19) invoke method Update() of DataAdapter to update data

        private void MainWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            _dtSource = this.GenerateDatable();
            _dtSource.DefaultView.RowFilter = "";
            Grid1.DataContext = _dtSource.DefaultView;
        }

        private void SaveButton_OnClick(object sender, RoutedEventArgs e)
        {
            var rowModified = _dtSource.AsEnumerable().Count(x => x.RowState == DataRowState.Modified);
            var rowAdded = _dtSource.AsEnumerable().Count(x => x.RowState == DataRowState.Added);
            var rowDeleted = _dtSource.AsEnumerable().Count(x => x.RowState == DataRowState.Deleted);
            if (rowModified == 0 && rowAdded == 0 && rowDeleted == 0)
            {
                MessageBox.Show($"You didn`t modify any row", "Information", MessageBoxButton.OK,
                    MessageBoxImage.Information);
                _dtSource.AcceptChanges();
            }
            else
            {
                MessageBox.Show($"You modifed grid", "Save changes ?", MessageBoxButton.YesNo, MessageBoxImage.Question);
            }
        }
    }
}


