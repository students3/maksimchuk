﻿namespace Business.Enums
{
    public enum SortingOrderEnum
    {
        ProductName,
        Price,
        Qty
    }
}
