﻿using Business.Enums;
using Business.Interfaces;
using System;

namespace Business
{
    public class MobilePhone : IProduct
    {
        public string ProductName { get; set; }
        public string Manufacturer { get; set; }
        public DateTime DateCreated { get; set; }
        public double Price { get; set; }
        public DateTime? DateSold { get; set; }
        public int Qty { get; set; }
        public SellQtyEnum SellQty => SellQtyEnum.Single;
        // Low, Medium, High
        public string Fragile { get; set; }

    }
}
