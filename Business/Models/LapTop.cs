﻿using Business.Interfaces;
using System;
using Business.Enums;

namespace Business
{
    public class LapTop : IProduct
    {
        public string ProductName { get; set; }
        public string Manufacturer { get; set; }
        public DateTime DateCreated { get; set; }
        public double Price { get; set; }
        public DateTime? DateSold { get; set; }
        public int Qty { get; set; }
        public SellQtyEnum SellQty => SellQtyEnum.InBatch5;
        public int GetZincQty()
        {
            return 19;
        }
    }
}
