﻿using System;
using Business.Interfaces;
using Business.Enums;

namespace Business
{
    public class Cake : IProduct
    {
        public string ProductName { get; set; }
        public string Manufacturer { get; set; }
        public DateTime DateCreated { get; set; }
        public double Price { get; set; }
        public int Qty { get; set; }
        public DateTime? DateSold { get; set; }

        public double Total { get; set; }
        public SellQtyEnum SellQty => SellQtyEnum.InBatch5;
        public int GetSugarQty()
        {
            return 2;
        }

        private string GetFullDescription()
        {
            return ProductName + " contains " + GetSugarQty() + " sugar";
        }
    }
}
