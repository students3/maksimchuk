﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Business.Interfaces;

namespace Business
{
    public class MobilePhoneManager : IManager
    {
        public void SellProduct(IProduct product, int qty)
        {
            product.DateCreated = DateTime.Now;
            product.Qty = qty;
        }
    }
}
