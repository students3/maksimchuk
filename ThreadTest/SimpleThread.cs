﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThreadTest
{
    class SimpleThread
    {

        public void Run()
        {
            var thread = new System.Threading.Thread(CalculateSum)
            {
                IsBackground = true
            };
            thread.Start();
            Console.WriteLine("Thread Start");
        }

        private static void CalculateSum()
        {
            System.Threading.Thread.Sleep(3000);
            var input = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            var sum = input.Sum();
            Console.WriteLine($"Sum is: {sum}");
        }
    }
}
