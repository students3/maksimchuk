﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadTest
{
    class ProductCache
    {
        public static List<Product> Products { get; set; }

        public static void UpdatePrice(int id, int price)
        {
            //lock (Products)
            //{
                var product = Products.FirstOrDefault(x => x.Id == id);
                if (product != null) product.Price = price;
            //}
        }
        public static void UpdateQty(int id, int qty)
        {
            //Monitor.Enter(Products);
                var product = Products.FirstOrDefault(x => x.Id == id);
                if (product != null) product.Qty = qty;
            //Monitor.Exit(Products);
        }
    }
}
