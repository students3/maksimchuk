﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThreadTest
{
    class Program
    {
        public static void Main(string[] args)
        {
            //var id = System.Threading.Thread.CurrentThread.ManagedThreadId;
            //Console.WriteLine("Main Thread ID = "+id);
            //SimpleThread st = new SimpleThread();
            //st.Run();
            System.Threading.Thread.Sleep(2000);
            ProductCache.Products = new List<Product>();
            ProductCache.Products.Add(new Product(){Id = 1, Name = "Soccerball", Price = 1, Qty = 1});
            var product = ProductCache.Products.FirstOrDefault(x => x.Id == 1);
            Console.WriteLine("Product price = "+product.Price);
            UpdatePriceConcurrent();
            Console.WriteLine("Product price = " + product.Price);
            Console.WriteLine("Press Any key");
            Console.ReadLine();
        }

        private static void UpdatePriceConcurrent()
        {
            var thread1 = new System.Threading.Thread(UpdatePrice5);
            var thread2 = new System.Threading.Thread(UpdatePrice10);
            var thread3 = new System.Threading.Thread(UpdatePrice20);
            var thread4 = new System.Threading.Thread(UpdatePrice10);

            thread1.Start();
            thread2.Start();
            thread3.Start();
            thread4.Start();


            while (thread1.IsAlive || thread2.IsAlive)
            {
                System.Threading.Thread.Sleep(2000);
            }
            Console.WriteLine("\r\n");
        }

        private static void UpdatePrice5()
        {
            ProductCache.UpdatePrice(1, 5);
            Console.WriteLine("Updated price to 5$");
        }
        private static void UpdatePrice10()
        {
            ProductCache.UpdatePrice(1, 10);
            Console.WriteLine("Updated price to 10$");
        }
        private static void UpdatePrice20()
        {
            ProductCache.UpdatePrice(1, 20);
            Console.WriteLine("Updated price to 20$");
        }

    }
}
