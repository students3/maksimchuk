﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime;
using System.Text;
using System.Threading.Tasks;

namespace ThreadTest
{
    class Product
    {
        public int Price { get; set; }
        public int Id { get; set; }
        public int Qty { get; set; }
        public string Name { get; set; }

    }
}
